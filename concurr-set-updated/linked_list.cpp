#include "linked_list.hpp"
#include "cTimer.h"

// ===================== Main =======================
int main(){
  // ===================== setup opencl components =====================
  int deviceId = 0;     // chosen device
  int platformId = 0;   // chosen platform

  createPlatform(&platform, platformId);
  createContext(&context, platform,&device, deviceId);
  createCommandQueue(context, device, &commandQueue);
  compileKernel("kernel.cl", "linkedlist", context, &program, &device, &kernel);

  // ===================== setup linked list and keys =====================

  std::string initListFile = "initList.txt";
  std::string keyfilename = "inputKeys.txt";

#if NEED_INPUT_GENERATOR
  createInitNodeFile(initListFile);
  createKeyFile(keyfilename);
#endif

  // set of unique keys that are initially
  std::set<int> unique_init_keys;

  // initialize a linked list with LISTSIZE initial nodes
  Node* list = createLinkedList(LISTSIZE, initListFile, unique_init_keys);

  std::cout << "unique_init_keys size " << unique_init_keys.size() << std::endl;

  // allocate a free memory space for all nodes to be inserted
  Node* freeMemPool = createFreeMemPool(WORKLOAD);

  // index of the next free slot in the free memory pool
  std::atomic<int>* nextFreeSlot = allocateSVM<std::atomic<int>>(1);
  *nextFreeSlot = 0;

  // ========= populate search keys ======
  // allocate and initialize a list of keys to be inserted
  int* keys = createKeys(keyfilename, WORKLOAD);

  // a list of per-thread results: SUCCESS or FAILURE
  int* results = allocateSVM<int>(WORKLOAD);

#if COUNT_ATOMIC
  int* failAtomicBuf = allocateSVM<int>(WORKLOAD);
  int* attemptAtomicBuf = allocateSVM<int>(WORKLOAD);
#endif

  for (int i = 0; i < WORKLOAD; i++) {
    results[i] = 0;
#if COUNT_ATOMIC
    failAtomicBuf[i] = 0;
    attemptAtomicBuf[i] = 0;
#endif
  }

#if USE_GPU
  int* devKeys = keys + CPU_WORK;  // &keys[CPU_WORK]
  int* devResults = results + CPU_WORK;
#if COUNT_ATOMIC
  int* devAttemptAtomicBuf = attemptAtomicBuf + CPU_WORK;
  int* devFailAtomicBuf = failAtomicBuf + CPU_WORK;
#endif

  // ========= set kernel arguments ======
  err = clSetKernelArgSVMPointer(kernel, 0, (void*)list);
  err = clSetKernelArgSVMPointer(kernel, 1, (void*)freeMemPool);
  err = clSetKernelArgSVMPointer(kernel, 2, (void*)devKeys);
  err = clSetKernelArgSVMPointer(kernel, 3, (void*)devResults);
  err = clSetKernelArgSVMPointer(kernel, 4, (void*)nextFreeSlot);
#if COUNT_ATOMIC
  err = clSetKernelArgSVMPointer(kernel, 5, (void*)devAttemptAtomicBuf);
  err = clSetKernelArgSVMPointer(kernel, 6, (void*)devFailAtomicBuf);
#endif
  // set up workgroup and global size
  wgsize = WG_SIZE;
  globalSize = GPU_WORK;
#endif

  // ========= start the execution =========
  std::cout << "Launching CPU worker threads ..." << std::endl;

  // create a vector of cpu threads
  std::vector<std::thread> cpu_threads;

  tHighResTimer timer;

#if USE_CPU
  for(unsigned int i = 0; i < CPU_THREADS; i++){
    std::cout << "spawning thread " << i << std::endl;

    cpu_threads.push_back(std::thread([=](){
      for (int j = 0; j < WORK_PER_CPU_THREAD; j++){
        int key = keys[i*WORK_PER_CPU_THREAD + j];

        // put key in the nextFreeSlot
        // link it to the list and return result
#if COUNT_ATOMIC
        results[i*WORK_PER_CPU_THREAD + j] = add(list, freeMemPool, key, nextFreeSlot, attemptAtomicBuf[i*WORK_PER_CPU_THREAD + j], failAtomicBuf[i*WORK_PER_CPU_THREAD + j]);
#else
        results[i*WORK_PER_CPU_THREAD + j] = add(list, freeMemPool, key, nextFreeSlot);
#endif
      }
    }));
  }
#endif

#if USE_GPU
  std::cout << "Launching GPU..." << std::endl;

  err = clEnqueueNDRangeKernel(commandQueue, kernel, 1, 0, &globalSize, &wgsize, 0, NULL, &event);

  if (err != CL_SUCCESS) {
    printf("Error in clEnqueueNDRangeKernel: %s\n",
    checkError(err));
    exit(-1);
  }

  // must do clFlush to guarantee all queued commands are issued to appropriate devices.
  err = clFlush(commandQueue);

  if (err != CL_SUCCESS) {
    printf("Error in clFlush: %s\n", checkError(err));
    exit(-1);
  }

  // stop GPU threads
  err = clFinish(commandQueue);

  if (err != CL_SUCCESS) {
    printf("Error in clFinish: %s\n",
    checkError(err));
    exit(-1);
  }
#endif

  // synchronize all cpu threads
  std::for_each(cpu_threads.begin(), cpu_threads.end(), [](std::thread &t){
    t.join();
  });

  std::cout << "Execution time " << timer.GetDuration()*1000.0 << "ms. " << std::endl;

  // ========= verify output =========
  if (!verifyOutput(list, keys, results, unique_init_keys)) {
    exit(-1);
  }

  std::cout << "Everything was correct" << std::endl;

  std::cout << "Printing list out " << std::endl;
  printList(list);

  int cpu_fail_acc = 0;
  int gpu_fail_acc = 0;
  int cpu_attempt_acc = 0;
  int gpu_attempt_acc = 0;
  int cpu_success_acc = 0;
  int gpu_success_acc = 0;

  for (int i = 0; i < WORKLOAD; i++) {
//    std::cout << "failAtomicBuf[" << i << "] = " << failAtomicBuf[i] ;
//    std::cout << " attemptAtomicBuf[" << i << "] = " << attemptAtomicBuf[i];
//    std::cout << " results[" << i << "] = " << results[i];

    if(i<CPU_WORK){
      cpu_fail_acc += failAtomicBuf[i];
      cpu_attempt_acc += attemptAtomicBuf[i];
      if(results[i] == 1){
        cpu_success_acc ++;
      }
    }else{
      gpu_fail_acc += failAtomicBuf[i];
      gpu_attempt_acc += attemptAtomicBuf[i];
      if(results[i] == 1){
        gpu_success_acc ++;
      }
    }
  }

  int total_fail = cpu_fail_acc + gpu_fail_acc;
  int total_attempt = cpu_attempt_acc + gpu_attempt_acc;
  int total_success = cpu_success_acc + gpu_success_acc;

  std::cout << "cpu fail: " << cpu_fail_acc << std::endl;

  std::cout << "(cpu) fail / success = " << (double) cpu_fail_acc / cpu_success_acc << std::endl;
  std::cout << "(gpu) fail / success = " << (double) gpu_fail_acc / gpu_success_acc << std::endl;

  std::cout << "(cpu) attempt / success = " << (double) cpu_attempt_acc / cpu_success_acc << std::endl;
  std::cout << "(gpu) attempt / success = " << (double) gpu_attempt_acc / gpu_success_acc << std::endl;

  std::cout << "(total) fail / success = " << (double) total_fail / total_success << std::endl;
  std::cout << "(total) attempt / success = " << (double) total_attempt / total_success << std::endl;



  std::ofstream atomiclog;
  atomiclog.open("atomiclog.txt", std::ios_base::app);

  atomiclog << "(cpu)fail/success = " << (double) cpu_fail_acc / cpu_success_acc ;
  atomiclog << " (gpu)fail/success = " << (double) gpu_fail_acc / gpu_success_acc ;

  atomiclog << " (cpu)attempt/success = " << (double) cpu_attempt_acc / cpu_success_acc ;
  atomiclog << " (gpu)attempt/success = " << (double) gpu_attempt_acc / gpu_success_acc ;

  atomiclog << " (total)fail/success = " << (double) total_fail / total_success ;
  atomiclog << " (total)attempt/success = " << (double) total_attempt / total_success<< std::endl;


  // ========= free memory ===========
  clSVMFree(context,list);
  clSVMFree(context,keys);
  clSVMFree(context,freeMemPool);
  clSVMFree(context,nextFreeSlot);
  clSVMFree(context,results);

  return 0;
}


//  double cpu_gpu_time = 0;
//  double c_g_begin = getTime();
//
//
//
//  //stop CPU worker threads
//  std::for_each(cpu_threads.begin(), cpu_threads.end(), [](std::thread &t){
//    t.join();
//  });
//
//  double c_g_end = getTime();
//  cpu_gpu_time = (c_g_end - c_g_begin);
//
//  printf("\n =================CPU + GPU====================\n");
//  printf("cpu and gpu share the workload, gpu does the first %d share, cpu does the second %d share.\n",GPUSHARE, CPUSHARE);
//  printf("total run time: %f microseconds\n", cpu_gpu_time);
//  // printf("cpu run time: %f microseconds\n", c_time);
//  // printf("join time: %f microseconds.\n", join_time);
//  printf("_________________________________________________\n");
//
//  // ========= Verify output =============
//
//    printf("========== After (linked list) ===========\n");
//#ifdef PRINTAFTER
//    printList(linkedListHead);
//#endif
//
//  printf("======================================\n");
//
//#ifdef PRINTOUTPUT
//  for (int i = 0; i < globalSize; i++){
//    printf(">>> thread %d, Action %d, key %d, output %d\n", i, hostActions[i], hostKeys[i], hostOutput[i]);
//  }
//#endif
//
//#ifdef CORRECTNESS
//  isCorrect(hostKeys, hostOutput, globalSize, linkedListHead, (hostKeys+globalSize)/*this should be the key for CPU*/, CPUresult);
//#endif
//
//  // Free memory space
//  free(linkedListHead);
//  free(freeMemPool);
//  free(nextFreeSlot);
//  if (hostKeys) free(hostKeys);
//  if (hostOutput) free(hostOutput);
//  // free(hostAddress);
