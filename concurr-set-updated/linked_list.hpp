#include <math.h>
#include <time.h>
#include <atomic>
#include <vector>
#include <omp.h>
#include <string>
#include <fstream>
#include <thread>
#include <algorithm> // need to include this library to enable std::for_each
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <set>
#include <map>
#include <algorithm>
#include <iostream>
#include <fstream>

#include "deviceSetup.hpp"

#define TRUE 1
#define FALSE 0
#define INVALID -99999999

#define CPU_THREADS 4      // number of CPU threads used

#define SUCCESS 1
#define FAILURE -1

#define LISTSIZE 1024          // initial list size
//#define LISTSIZE 0
#define WORK_UNIT 131072
// #define WORK_UNIT 32768
//#define WORK_UNIT 1024

#define WORKLOAD 10*WORK_UNIT  // number of keys to be inserted
//#define WORKLOAD WORK_UNIT
//#define MAXKEY 10000           // range of key's value
#define MAXKEY 10000

#define USE_GPU 1
#define USE_CPU 1
#define COUNT_ATOMIC 1

#if !USE_GPU
  #define CPU_WORK WORKLOAD
  #define GPU_WORK 0
#elif !USE_CPU
  #define CPU_WORK 0
  #define GPU_WORK WORKLOAD
#else
  #define CPU_SHARE_RATIO 2
  #define CPU_WORK WORK_UNIT*CPU_SHARE_RATIO
  #define GPU_WORK WORKLOAD-CPU_WORK
#endif

#define WORK_PER_CPU_THREAD CPU_WORK/CPU_THREADS
#define NEED_INPUT_GENERATOR 1

#define WG_SIZE 64

//=========================
typedef struct Node{
  // A node contains: value, address of the next node and mark bite4r
  // first 48 bits is the address of next node,
  // and the next 16 bits is an unsigned 0 or 1 integer for mark bit
  // (logically but not yet physically removed)
  int value;
  std::atomic<ulong> nextAndMark;
} Node;

typedef struct Window {
  Node* pred;
  Node* curr;
} Window;

//=============================== helper function that combines pointer and markbit ==============================================
// combine an address and mark bit into one single unsigned long number
unsigned long combine(Node* pointer, unsigned short markbit){
  return ((unsigned long) pointer << 16 | ((unsigned long) markbit));
}

Node* extractPointer(std::atomic<ulong>* nextAndMark){
  unsigned long myNextAndMark = atomic_load_explicit(nextAndMark, std::memory_order_relaxed);
  return (Node*)(myNextAndMark >> 16);
}

unsigned short extractMarkbit(std::atomic<ulong>* nextAndMark){
  unsigned long myNextAndMark = atomic_load_explicit(nextAndMark, std::memory_order_relaxed);
  return (unsigned short) (myNextAndMark & 0x000000000000FFFF);
}

// print the list
void printList(Node* head){
  Node* curr = head;

  int listsize = 0;

  while (curr != NULL){
//    std::cout << "Value " << curr->value
//                << " marked bit " << extractMarkbit(&(curr->nextAndMark))
//                << " next node address " << extractPointer(&(curr->nextAndMark))
//                << std::endl;

    curr = extractPointer(&(curr->nextAndMark));
    listsize ++;
  }

  std::cout << "list size: " << listsize << std::endl;
}

//=============================== actions on the list, add, remove and search    =================================================
#if COUNT_ATOMIC
Window find(Node* linkedList, int searchKey, int &attempt, int &fail){
  Node* pred = NULL;
  Node* curr = NULL;
  Node* succ = NULL;

  bool snip;
  pred = linkedList;

  tryagain: pred = linkedList;
  curr = extractPointer(&(pred->nextAndMark));

  Window newWindow; // returned region (i.e., window)

  do {
    if(curr != NULL) {
      succ = extractPointer(&(curr->nextAndMark));

      while (curr != NULL && extractMarkbit(&(curr->nextAndMark)) == 1) { // as long as current node is marked --> remove it
        // we expect the current node (both address and markbit) is unchanged
        unsigned long expected = combine(curr,0); // next node is curr and pred is clean
        unsigned long desired = combine(succ,0);  // next node is succ and pred is clean

        // atomically compare our expected (current address + mark bit) with the real (current address + mark bit)
        // if it's as expected --> update the pred->nextAndMark to the succ node (physically remove the curr)
        // otherwise --> snip failed --> retry (i.e., someone else already modified either address or mark bit of the curr)
        attempt++;
        snip = atomic_compare_exchange_strong(&(pred->nextAndMark), &expected, desired); // compare value, expected value, desired value;

        if(!snip){
          fail++;
          goto tryagain;
        }

        // TODO put the removed node to the garbage

        // update curr
        curr = succ;
        if (curr != NULL)
          succ = extractPointer(&(curr->nextAndMark));
      }
    }

    /* after the above loop --> all marked nodes before this point must be removed */

    newWindow.pred = pred;
    newWindow.curr = curr;

    // move on to the next node
    pred = curr;
    if(curr != NULL){
      curr = extractPointer(&(curr->nextAndMark));
    }
  // loop until the previous curr (i.e., pred now) is NULL or its value is greater than or equal to the search key
  } while (pred != NULL && pred->value < searchKey);  //question: used to be while loop, now do-while, used to be curr != NULL, now pred. is it because we changed head node from 0 to head.

  return newWindow;
}

int add(Node* linkedList, Node* freeMemPool, int insertValue, std::atomic<int>* nextFreeSlot, int &attempt, int &fail/*, int* counter*/){
  // get my slot and update the global nextFreeSlot
  // TODO if I get my slot first, the slot may not be used if the function returns FAIL --> FIX IT
  // if I get my slot after getting the window, each time I retry, new slot will be given to me

  int myslot = atomic_fetch_add_explicit(nextFreeSlot, 1, std::memory_order_relaxed);

  while (true) {
    // find the region/window (pred, curr) to add the insertValue
    Window window = find(linkedList, insertValue, attempt, fail);

    if (window.curr != NULL && window.curr->value == insertValue){
      // the insertValue is already in the linked list
      // no duplicated value is allowed in a set --> failed
      return FAILURE;
    }

    /* otherwise, add the insertValue in between pred and curr */

    // write the insertValue to new node
    freeMemPool[myslot].value = insertValue;

    // update the nextAndMark field of new node
    unsigned long desired = combine(window.curr,0);

    atomic_store_explicit(&(freeMemPool[myslot].nextAndMark),desired,std::memory_order_seq_cst);

    // link the new node to the linked list
    unsigned long expected = combine(window.curr,0);
    desired = combine(freeMemPool+myslot,0);
    attempt++;
    int ret = atomic_compare_exchange_strong_explicit(&(window.pred->nextAndMark), &expected, desired, std::memory_order_seq_cst, std::memory_order_relaxed);

    if (ret == 0) {
      fail++;
      continue;
    }

    return SUCCESS;
  }
}
#else
Window find(Node* linkedList, int searchKey){
  Node* pred = NULL;
  Node* curr = NULL;
  Node* succ = NULL;

  bool snip;
  pred = linkedList;

  tryagain: pred = linkedList;
  curr = extractPointer(&(pred->nextAndMark));

  Window newWindow; // returned region (i.e., window)

  do {
    if(curr != NULL) {
      succ = extractPointer(&(curr->nextAndMark));

      while (curr != NULL && extractMarkbit(&(curr->nextAndMark)) == 1) { // as long as current node is marked --> remove it
        // we expect the current node (both address and markbit) is unchanged
        unsigned long expected = combine(curr,0); // next node is curr and pred is clean
        unsigned long desired = combine(succ,0);  // next node is succ and pred is clean

        // atomically compare our expected (current address + mark bit) with the real (current address + mark bit)
        // if it's as expected --> update the pred->nextAndMark to the succ node (physically remove the curr)
        // otherwise --> snip failed --> retry (i.e., someone else already modified either address or mark bit of the curr)
        snip = atomic_compare_exchange_strong(&(pred->nextAndMark), &expected, desired); // compare value, expected value, desired value;

        if(!snip){
          goto tryagain;
        }

        // TODO put the removed node to the garbage

        // update curr
        curr = succ;
        if (curr != NULL)
          succ = extractPointer(&(curr->nextAndMark));
      }
    }

    /* after the above loop --> all marked nodes before this point must be removed */

    newWindow.pred = pred;
    newWindow.curr = curr;

    // move on to the next node
    pred = curr;
    if(curr != NULL){
      curr = extractPointer(&(curr->nextAndMark));
    }
  // loop until the previous curr (i.e., pred now) is NULL or its value is greater than or equal to the search key
  } while (pred != NULL && pred->value < searchKey);  //question: used to be while loop, now do-while, used to be curr != NULL, now pred. is it because we changed head node from 0 to head.

  return newWindow;
}

int add(Node* linkedList, Node* freeMemPool, int insertValue, std::atomic<int>* nextFreeSlot/*, int* counter*/){
  // get my slot and update the global nextFreeSlot
  // TODO if I get my slot first, the slot may not be used if the function returns FAIL --> FIX IT
  // if I get my slot after getting the window, each time I retry, new slot will be given to me

  int myslot = atomic_fetch_add_explicit(nextFreeSlot, 1, std::memory_order_relaxed);

  while (true) {
    // find the region/window (pred, curr) to add the insertValue
    Window window = find(linkedList, insertValue);

    if (window.curr != NULL && window.curr->value == insertValue){
      // the insertValue is already in the linked list
      // no duplicated value is allowed in a set --> failed
      return FAILURE;
    }

    /* otherwise, add the insertValue in between pred and curr */

    // write the insertValue to new node
    freeMemPool[myslot].value = insertValue;

    // update the nextAndMark field of new node
    unsigned long desired = combine(window.curr,0);
    atomic_store_explicit(&(freeMemPool[myslot].nextAndMark),desired,std::memory_order_seq_cst);

    // link the new node to the linked list
    unsigned long expected = combine(window.curr,0);
    desired = combine(freeMemPool+myslot,0);

    int ret = atomic_compare_exchange_strong_explicit(&(window.pred->nextAndMark), &expected, desired, std::memory_order_seq_cst, std::memory_order_relaxed);

    if (ret == 0) continue;

    return SUCCESS;
  }
}
#endif
// ============== Initiate SVM objects ==============
// Linked list is shared by both CPU and GPU
Node* createLinkedList(int listSize, std::string fileName, std::set<int>& unique_init_keys){
  // open input file
  std::ifstream myfile(fileName);

  if (!myfile.is_open()){
    std::cout << "error in opening the file" << std::endl;
    exit(-1);
  }

  // first node is the HEAD --> its value is INVALID
  int num_node = (listSize + 1);

  // create an SVM array of nodes
  Node* linkedListHead = allocateSVM<Node>(num_node);

  if (linkedListHead == nullptr){
    printf("Error in allocating the list\n");
    exit(-1);
  }

  // instantiate the HEAD
  linkedListHead->value = INVALID;

  atomic_store_explicit(&(linkedListHead->nextAndMark), combine(NULL,0), std::memory_order_relaxed);

  Node* val_ptr = extractPointer(&(linkedListHead->nextAndMark));
  if (val_ptr != NULL) exit(-1);

  Node* currNode = linkedListHead;

  // fill in value of each element, and make links between elements.
  for (int i = 0; i < listSize; i++){
    int markBit = 0;
    if (i != listSize - 1){
      currNode->nextAndMark = combine(currNode+1,markBit);
      currNode = currNode + 1;
      myfile >> currNode->value;

      // insert the value to the unique key set
      unique_init_keys.insert(currNode->value);
    } else {
      currNode->nextAndMark = combine(NULL, markBit);
    }
  }

  // close input file
  myfile.close();

  return linkedListHead;
}

//================================ initialization for linked list. ==============================================================
// Free memory pool is the list of already allocated empty nodes
// if GPU threads need to insert new node --> get allocated memory from here
Node* createFreeMemPool(int poolSize){
  Node* pool = allocateSVM<Node>(poolSize);

  if (!pool) {
    std::cout << "Failed to allocate free memory pool" << std::endl;
    exit(-1);
  }

  return pool;
}

// list of keys to be searched in GPU
int* createKeys(std::string filename, int numKeys){
  int* hostKeys = allocateSVM<int>(numKeys);

  std::ifstream myfile(filename);

  if(!myfile.is_open()){
    printf("problem opening file!!");
    exit(-1);
  }

  for (int i = 0; i < numKeys; i++){
    myfile >> hostKeys[i];
	  //hostKeys[i] = i % MAXKEY + i;
  }

  myfile.close();

  return hostKeys;
}

// =============== verify outputs =================
bool verifyOutput(Node* list, int* keys, int* results, std::set<int> unique_init_keys){
//  for (std::set<int>::iterator iter = unique_init_keys.begin(); iter != unique_init_keys.end(); iter++)
//    std::cout << *iter << std::endl;

  bool ret = true;

  // create a unordered_map of keys and results
  std::map<int, std::vector<int>> maps;

  for (int i = 0; i < WORKLOAD; i++){
    // find the key in maps
    std::map<int, std::vector<int>>::iterator elem = maps.find(keys[i]);

    if (elem == maps.end()){
      // the key is not existing in the maps
      // make a new entry in the map
      std::vector<int> value;
      value.push_back(results[i]);
      maps.insert(std::make_pair(keys[i], value));
    } else {
      // add the result to the end of corresponding list for that elem
      elem->second.push_back(results[i]);
    }
  }

//  for (std::map<int, std::vector<int>>::iterator iter = maps.begin(); iter != maps.end(); iter++){
//    std::cout << "Key " << iter->first << ": ";
//    std::vector<int> vec = iter->second;
//
//    for (std::vector<int>::iterator vec_iter = vec.begin(); vec_iter != vec.end(); vec_iter++){
//      std::cout << *vec_iter << ", ";
//    }
//
//    std::cout << std::endl;
//  }

  // go through each element in the map
  // see if there is a duplicate success
  for (std::map<int, std::vector<int>>::iterator iter = maps.begin(); iter != maps.end(); iter++){
    const int key = iter->first;
    std::vector<int> result_list = iter->second;
    int count = std::count(result_list.begin(),result_list.end(),SUCCESS);

    if (unique_init_keys.find(key) != unique_init_keys.end()){
      // the key is initially in the list

      // the number of SUCCESS should be equal to the size of result_list
      if (count != 0){
        std::cout << "There is duplicate insertion for key " << key
                      << " that is initially in the list."
                      << " count = " << count
                      << std::endl;
        ret = false;
      }
    } else {
      // the key is not initially in the list
      if (count != 1){
        std::cout << "There is a duplicate insertion or no insertion for key " << key
                        << " that is not initially in the list"
                        << " count = " << count
                        << std::endl;
        ret = false;
      }
    }
  }

  return ret;
}

// create init nodes
void createInitNodeFile(std::string fileName){
  std::ofstream myfile;
  myfile.open(fileName);

  for (int i = 0; i < LISTSIZE; i++){
    myfile << i * 100;
    if (i != LISTSIZE - 1) myfile << "\n";
  }

  myfile.close();
}

// create keys
void createKeyFile(std::string fileName){
  std::ofstream myfile;
  myfile.open(fileName);

  for (int i = 0; i < WORKLOAD; i++){
    myfile << rand() % MAXKEY;
    if (i != WORKLOAD - 1) myfile << "\n";
  }

  myfile.close();
}
