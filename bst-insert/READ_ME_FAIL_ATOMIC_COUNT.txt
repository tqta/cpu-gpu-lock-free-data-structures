To turn on counting atomic failure function, please modify the following files:

1. Go to SVMAtomicsBinaryTreeInsert.hpp, line 38, define COUNT_ATOMIC as 1 
2. Go to SVMBinaryNode.h, line 1, define COUNT_ATOMIC as 1

To turn off atomic failure counting function, define above 2 macro as 0;