#!/bin/bash
# A script to run bst-insert binary with various arguments
# Xiaoqi Hu. 10/15/2016 5:14pm



num_insert=50000

for host_percent in {0..100..20}
do
    rm -f ./atomiclog.txt*
    rm -f ./output.txt
    for i in {1..20}
    do
	    ./bin/x86_64/Release/SVMAtomicsBinaryTreeInsert -n $num_insert -hp $host_percent >> output.txt
	    #tail -1 ./atomiclog.txt
    done

    awk '{attempt += $3; fail += $6; count++} END { printf "attempt average: %f, fail average: %f, number of iterations: %d\n", attempt/NR, fail/NR, count}' atomiclog.txt 
done

echo All done!

