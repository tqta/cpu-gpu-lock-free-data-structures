#include "linked_list.hpp"
#include "cTimer.h"

// ===================== Main =======================
int main(){
  // ===================== setup opencl components =====================
  int deviceId = 0;     // chosen device
  int platformId = 0;   // chosen platform

  createPlatform(&platform, platformId);
  createContext(&context, platform,&device, deviceId);
  createCommandQueue(context, device, &commandQueue);
  compileKernel("kernel.cl", "linkedlist", context, &program, &device, &kernel);

  // ===================== setup linked list and keys =====================

  std::string initListFile = "initList.txt";

  // set of unique keys that are initially
  std::set<int> unique_init_keys;

  // initialize a linked list with LISTSIZE initial nodes
  Node* list = createLinkedList(LISTSIZE, initListFile, unique_init_keys);

  std::cout << "unique_init_keys size " << unique_init_keys.size() << std::endl;

  // allocate a free memory space for all nodes to be inserted
  Node* freeMemPool = createFreeMemPool(WORKLOAD);

  // index of the next free slot in the free memory pool
  std::atomic<int>* nextFreeSlot = allocateSVM<std::atomic<int>>(1);
  *nextFreeSlot = 0;

  // ========= populate search keys ======
  // allocate and initialize a list of keys to be inserted
  int* keys = createKeys("dummy file", WORKLOAD);

  // a list of per-thread results: SUCCESS or FAILURE
  int* results = allocateSVM<int>(WORKLOAD);

  for (int i = 0; i < WORKLOAD; i++) {
    results[i] = 0;
  }

#if USE_GPU
  int* devKeys = keys + CPU_WORK;  // &keys[CPU_WORK]
  int* devResults = results + CPU_WORK;

  // ========= set kernel arguments ======
  err = clSetKernelArgSVMPointer(kernel, 0, (void*)list);
  err = clSetKernelArgSVMPointer(kernel, 1, (void*)freeMemPool);
  err = clSetKernelArgSVMPointer(kernel, 2, (void*)devKeys);
  err = clSetKernelArgSVMPointer(kernel, 3, (void*)devResults);
  err = clSetKernelArgSVMPointer(kernel, 4, (void*)nextFreeSlot);

  // set up workgroup and global size
  wgsize = WG_SIZE;
  globalSize = GPU_WORK;
#endif

  // ========= start the execution =========
  std::cout << "Launching CPU worker threads ..." << std::endl;

  // create a vector of cpu threads
  std::vector<std::thread> cpu_threads;

  tHighResTimer timer;

#if USE_CPU
  for(unsigned int i = 0; i < CPU_THREADS; i++){
    std::cout << "spawning thread " << i << std::endl;

    cpu_threads.push_back(std::thread([=](){
      for (int j = 0; j < WORK_PER_CPU_THREAD; j++){
        int key = keys[i*WORK_PER_CPU_THREAD + j];

        // put key in the nextFreeSlot
        // link it to the list and return result
        results[i*WORK_PER_CPU_THREAD + j] = add(list, freeMemPool, key, nextFreeSlot);
      }
    }));
  }
#endif

#if USE_GPU
  std::cout << "Launching GPU..." << std::endl;

  err = clEnqueueNDRangeKernel(commandQueue, kernel, 1, 0, &globalSize, &wgsize, 0, NULL, &event);

  if (err != CL_SUCCESS) {
    printf("Error in clEnqueueNDRangeKernel: %s\n",
    checkError(err));
    exit(-1);
  }

  // must do clFlush to guarantee all queued commands are issued to appropriate devices.
  err = clFlush(commandQueue);

  if (err != CL_SUCCESS) {
    printf("Error in clFlush: %s\n", checkError(err));
    exit(-1);
  }

  // stop GPU threads
  err = clFinish(commandQueue);

  if (err != CL_SUCCESS) {
    printf("Error in clFinish: %s\n",
    checkError(err));
    exit(-1);
  }
#endif

  // synchronize all cpu threads
  std::for_each(cpu_threads.begin(), cpu_threads.end(), [](std::thread &t){
    t.join();
  });

  std::cout << "Execution time " << timer.GetDuration()*1000.0 << "ms. " << std::endl;

  // ========= verify output =========
  if (!verifyOutput(list, keys, results, unique_init_keys)) {
    exit(-1);
  }

  std::cout << "Everything was correct" << std::endl;

  std::cout << "Printing list out " << std::endl;
  printList(list);

  // ========= free memory ===========
  clSVMFree(context,list);
  clSVMFree(context,keys);
  clSVMFree(context,freeMemPool);
  clSVMFree(context,nextFreeSlot);
  clSVMFree(context,results);

  return 0;
}
