# Heterogeneous Systems Research (HEROES) Lab - University of Mississippi #

This is a collection of concurrent data structures ported to APU and GPU platforms and applications that
use those data structures.

## List of data structures (updated on Sep 25) ##

Blocking FIFO queue
Lock-free FIFO queue
Lock-free concurrent set (based on concurrent linked list data structure)

## Applications ##

CPU-GPU work-stealing framework using shared lock-free FIFO queues to communicate tasks between GPU workers
and CPU workers in a shared memory system.

## Platform requirements ##

For now, our code works on AMD APU Kaveri platform. Futute support for Shared Virtual Memory (SVM) discrete GPU
systems will be released.