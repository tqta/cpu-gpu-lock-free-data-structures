#include "oclDeviceSetup.hpp"
#include "Deque.hpp"

bool pushBottom(Deque* deque, const Task &newItem){
  unsigned int oldBottom = deque->bottom;
  unsigned int oldTop = deque->top.load(std::memory_order_acquire);
  int size = oldBottom - oldTop;

  // check boundary condition
  if (size >= CIRCULAR_ARRAY_CAPACITY){
    // no more space in internal circular array
    return false;
  }

  // simply add the new item to the bottom position
  put(deque->array, oldBottom, newItem);

  // increment bottom
  deque->bottom = oldBottom + 1;

  // release fence flushing data to shared memory
  std::atomic_thread_fence(std::memory_order_release);

  return true;
}

void popBottom(Deque* deque, Task* returnItem){
  deque->bottom--;      // decrement bottom

  // TODO Is this fence necessary?
  // release fence flushing data to shared memory
  std::atomic_thread_fence(std::memory_order_release);

  unsigned int oldTop = deque->top.load(std::memory_order_acquire);
  unsigned int newTop = oldTop + 1;
  int size = deque->bottom - oldTop;

  if (size < 0) {                 // no more item in the deque
    deque->bottom = oldTop;       // set bottom to oldTop

    // TODO Is this fence necessary?
    // release fence flushing data to shared memory
    std::atomic_thread_fence(std::memory_order_release);

    returnItem = nullptr;
    return;
  }

  Task item = get(deque->array, deque->bottom);

  if (size > 0){
    // there is more than one item in the deque
    // it's safe to return the item at the bottom
    *returnItem = item;
    return;
  }

  // if we reach to this point, there is likely only one item in the deque
  // we must use atomic CAS to safely update the top
  if (!deque->top.compare_exchange_weak(oldTop, newTop, std::memory_order_seq_cst, std::memory_order_relaxed)){
    // CAS failed --> a thief already stole the last item
    // return nullptr
    returnItem = nullptr;
  }

  // either I or thief got the last item
  // bottom is updated to newTop anyway b/c the queue is now empty
  deque->bottom = newTop;

  // release fence flushing data to shared memory
  std::atomic_thread_fence(std::memory_order_release);

  return;
}

void popTop(Deque* deque, Task* returnItem){
  unsigned int oldTop = deque->top.load(std::memory_order_acquire);
  unsigned int newTop = oldTop + 1;
  unsigned int oldBottom = deque->bottom;

  int size = oldBottom - oldTop;

  if (size <= 0) {
    // empty deque
    returnItem = nullptr;
    return;
  }

  Task item = get(deque->array, oldTop);

  // compete with the owner to get this item
  if (deque->top.compare_exchange_weak(oldTop, newTop, std::memory_order_seq_cst, std::memory_order_relaxed)){
    *returnItem = item;
  } else {
    returnItem = nullptr;
  }
}

bool isEmpty(Deque* deque){
  unsigned int localTop = deque->top.load(std::memory_order_acquire);
  unsigned int localBottom = deque->bottom;
  return (localBottom <= localTop);
}
