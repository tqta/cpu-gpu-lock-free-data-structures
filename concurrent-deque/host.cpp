/*
 * Implement task queuing framework for heterogeneous CPU/GPU platform
 * Each thread (either CPU or GPU thread) is able to pull task from
 * the queue, execute task and probably produce task into the queue
 */

#include <atomic>
#include <chrono>
#include <thread>
#include <vector>
#include <unordered_map>

#include "oclDeviceSetup.hpp"
#include "SharedMacros.h"

#include "cTimer.h"
#include <cmath>
#include <list>

void test(int tid, Deque* parWorkQueues, unsigned int* parOutput, std::atomic_uint* nActiveQueues){
  Deque &myQueue = parWorkQueues[tid];

  bool done = false;

  do {
    // popBottom: if returnTask == nullptr, then decrement nActiveQueues

    // if returnTask == nullptr
    //        decrement nActiveQueues
    //        find victim queue --> try to steal

    // execute the task if any


  } while (!done);
}

int main(){
//	// =============================== opencl vars ===============================
//	cl_platform_id platform;
//	cl_context context;
//	cl_device_id device;
//	cl_command_queue commandQueue;
//	cl_program program;
//	cl_kernel kernel;
//	cl_event event;
//
//	size_t wgsize = 256;
//	size_t globalSize = wgsize * 40;
//  size_t nCPUThreads = 4;   // number of CPU threads
//
//  // ===================== setup opencl components =====================
//  int deviceId = 0;     // chosen device
//  int platformId = 0;   // chosen platform
//
//  createPlatform(platform, platformId);
//  createContext(context, platform, device, deviceId);
//  createCommandQueue(context, device, commandQueue);
//  compileKernel("Kernel.cl", "concurrentQueue", context, program, device, kernel);

  /* -------------------------- A mini test ---------------------------
   * Task structure:
   *        - TaskID: unsigned int [0..N-1]
   * Each task does:
   *        - atomically increment output[TaskID]
   *        - generate (N-TaskID) new tasks & new task IDs = TaskID+1 ... TaskID+(N-TaskID)
   * ------------------------------------------------------------------
   */

  // ===================== allocate structures =====================
  // output array has N slots
  unsigned int seqOutput[OUTPUT_RANGE];
  unsigned int parOutput[OUTPUT_RANGE];

  for (unsigned int i = 0; i < OUTPUT_RANGE; ++i){
    seqOutput[i] = 0;
    parOutput[i] = 0;
  }

  // an array of nThreads work queues
  // each work queue is associated with a thread
  std::list<Task> seqWorkQueues[N_THREADS];    // SEQUENTIAL version
  Deque parWorkQueues[N_THREADS];              // PARALLEL version

  // initialize all queues
  for (unsigned int i = 0; i < N_THREADS; ++i){
    Task randTask = rand()%THRESHOLD;

    // initialize seqWorkQueues
    seqWorkQueues[i].push_back(randTask);

    // initialize parWorkQueues
    Deque &myParQueue = parWorkQueues[i];
    myParQueue.array = (Task*) malloc(sizeof(Task) * CIRCULAR_ARRAY_CAPACITY);
    myParQueue.top.store(0);
    myParQueue.bottom = 0;
    myParQueue.noMoreTask = false;

    pushBottom(parWorkQueues+i, randTask);
  }

  // number of queues that have at least one task
  std::atomic_uint nActiveQueues;
  nActiveQueues.store(N_THREADS);   // initially all threads have task to execute

  // ===================== Parallel execution =====================
  // start CPU threads
  std::vector<std::thread*> threads;

  for (size_t tid = 0u; tid < N_THREADS; ++tid){

  }





  // ===================== Sequential execution =====================
  // process tasks
  for (unsigned int tid = 0; tid < N_THREADS; ++tid){
    std::list<Task> &myQueue = seqWorkQueues[tid];

    while (!myQueue.empty()){
      // pull a task from myQueue
      Task taskID = myQueue.front();
      myQueue.pop_front();

      // execute task
      seqOutput[((taskID+tid)*RANDOM_FACTOR) % OUTPUT_RANGE]++;

      // spawn at most branchingFactor new tasks
      Task newID = 0;
      for (unsigned int i = 1; i <= BRANCHING_FACTOR; i++){
        newID = taskID + i;

        if (newID < THRESHOLD)              // only push valid newID to the queue
          myQueue.push_back(newID);
      }
    }
  }

  for (unsigned int i = 0; i < OUTPUT_RANGE; i++)
    std::cout << "i = " << i << ": " << seqOutput[i] << std::endl;

//  // ===================== free opencl objects =====================
//  freeCLObjects(commandQueue, context, program, kernel);

  return 0;
}
