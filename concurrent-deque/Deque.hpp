#ifndef DEQUE_HPP
#define DEQUE_HPP

#include "SharedMacros.h"
#include <atomic>

/*
 * Data could be separate structure
 * Use "int" type for simplicity
 */
typedef unsigned int Task;

typedef struct {
  Task* array;                        // pointer to internal circular array
  std::atomic_uint top;               // top index
  volatile unsigned int bottom;       // bottom index (no atomic type needed b/c of no data race)
  volatile bool noMoreTask;           // whether this deque is going to produce more tasks
} Deque;

/*
 * The Deque's owner calls this function
 * -------------------------------------
 * push a new item to the bottom of a given deque
 *      return true if newItem was successfully added
 *      otherwise return false (overflow in internal circular array)
 */
bool pushBottom(Deque* deque, const Task &newItem);

/*
 * The deque's owner calls this function
 * -------------------------------------
 * pop an element from the bottom of a given deque
 * returnItem is nullptr if the operation fails
 */
void popBottom(Deque* deque, Task* returnItem);

/*
 * A thief calls this function
 * -------------------------------------
 * pop an element from the bottom of a given deque
 * returnItem is nullptr if the operation fails
 */
void popTop(Deque* deque, Task* returnItem);

/*
 * check if a given deque is empty
 */
bool isEmpty(Deque* deque);

/*
 * put a new item in a specific place in the internal circular array
 */
inline void put(Task* array, unsigned int index, const Task &newItem){
  array[index % CIRCULAR_ARRAY_CAPACITY] = newItem;
};

/*
 * get an item from the internal circular array
 */
inline Task get(Task* array, unsigned int index){
  return array[index % CIRCULAR_ARRAY_CAPACITY];
}

#endif
