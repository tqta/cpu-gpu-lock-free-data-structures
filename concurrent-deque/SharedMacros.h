/*
 * List of macros shared by both host and kernel code
 */

#define CIRCULAR_ARRAY_CAPACITY 10000   // maximum number of items in a circular array

// test case configurations
#define OUTPUT_RANGE     50
#define THRESHOLD        50
#define BRANCHING_FACTOR 2
#define RANDOM_FACTOR    179426549
#define N_THREADS        4
