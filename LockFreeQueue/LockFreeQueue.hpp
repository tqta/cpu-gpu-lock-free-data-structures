#ifndef LOCKFREEQUEUE_HPP_
#define LOCKFREEQUEUE_HPP_

#include "oclDeviceSetup.hpp"
#include "Structures.hpp"

/*
 * construct and initialize an empty list
 */
LockFreeQueue* constructQueue(const cl_context &context, FreePool* memPool);

/*
 * enqueue: insert newData to a given queue in lock-free manner
 *          return true if newData is successfully inserted
 *          return false otherwise (e.g., 16-bit counter overflows)
 * assume: newNode is allocated by the caller
 */
bool enqueue(const cl_context &context, LockFreeQueue* queue, Node* newNode, int tid, PerfCount* perfCount);

/*
 * dequeue: pop the head element from a given queue in lock-free manner
 *
 */
bool dequeue(const cl_context &context, FreePool* memPool, LockFreeQueue* queue, Task* returnData, int tid, PerfCount* perfCount);

/*
 * free up memory space for the queue
 * assume: the queue has been drained
 */
void deallocateQueue(const cl_context &context, FreePool* memPool, LockFreeQueue* queue);

/*
 * Combine a 64-bit node pointer and unsigned int counter into a TaggedPointer
 * Assume: caller already checks boundary condition of counter
 */
inline Pointer getTaggedPointer(Node* address, unsigned int counter){
  return ((Pointer)address << 16) | (Pointer) counter;
};

/*
 * Extract address field of a tagged pointer
 */
inline Node* getAddressFromTaggedPtr(Pointer taggedPtr){
  return (Node*)(taggedPtr >> 16);
};

/*
 * Extract counter field of a tagged pointer
 */
inline unsigned int getCounterFromTaggedPtr(Pointer taggedPtr){
  return (unsigned int)(taggedPtr & 0x000000000000FFFF);
};

#endif /* LOCKFREEQUEUE_HPP_ */
