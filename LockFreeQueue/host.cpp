/*
 * Implement task queuing framework for heterogeneous CPU/GPU platform
 * Each thread (either CPU or GPU thread) is able to pull task from
 * the queue, execute task and probably produce task into the queue
 */

#include <atomic>
#include <chrono>
#include <thread>
#include <vector>
#include <unordered_map>
#include "math.h"

#include "oclDeviceSetup.hpp"
#include "Structures.hpp"
#include "FreeMemPool.hpp"

#include "cTimer.h"
#include <cmath>
#include "LockFreeQueue.hpp"

#include <iostream>
#include <fstream>

void test(int tid, std::unordered_map<Task, int> &myMap, cl_context &context, FreePool* memPool, LockFreeQueue* queue, PerfCount* perfCount);
void prepareGPUTest(const cl_context &context, int* actions, int* newValues, int* poppedNodes);
void processGPUOutput(int* actions, int* newValues, int* poppedNodes, std::unordered_map<Task, int>* maps);

#define REPEAT 3      // number of runs

int main(){
  // =============================== opencl vars ===============================
  cl_platform_id platform;
  cl_context context;
  cl_device_id device;
  cl_command_queue commandQueue;
  cl_program program;
  cl_kernel kernel;

  // ===================== setup opencl components =====================
  int deviceId = 0;     // chosen device
  int platformId = 0;   // chosen platform

  createPlatform(platform, platformId);
  createContext(context, platform, device, deviceId);
  createCommandQueue(context, device, commandQueue);
  compileKernel("Kernel.cl", "concurrentQueue", context, program, device, kernel);

#if nWorkGroups
  cl_event event;
  size_t wgsize = WG_SIZE;
  size_t globalSize = NDRANGE_SIZE;
#endif

  std::ofstream outputFile ("output.txt");
  assert(outputFile.is_open());

  for (unsigned int iter = 0; iter < REPEAT; ++iter){
    std::cout << "Run #" << iter << std::endl;

    // ===================== allocate structures =====================
    // create a free memory pool
    FreePool* memPool = constructMemPool(context);

    // create & initialize a queue
    LockFreeQueue* queue = constructQueue(context, memPool);

#if nWorkGroups
    // extra structures for GPU kernel
    int* actions = (int*) allocateSVM<int>(context, nGPUTasks);
    assert(actions);

    int* newValues = (int*) allocateSVM<int>(context, nGPUTasks);
    assert(newValues);

    int* poppedNodes = (int*) allocateSVM<int>(context, nGPUTasks);
    assert(poppedNodes);
#endif

    int* poolOverflow = (int*) allocateSVM<int>(context, 1);
    assert(poolOverflow);
    poolOverflow[0] = 0;

    int* counterOverflow = (int*) allocateSVM<int>(context, 1);
    assert(counterOverflow);
    counterOverflow[0] = 0;

    std::atomic_int* gateOpened = (std::atomic_int*) allocateSVM<std::atomic_int>(context, 1);
    assert(gateOpened);
    gateOpened[0] = 0;

    PerfCount* perfCountList = (PerfCount*) allocateSVM<PerfCount>(context, NDRANGE_SIZE+nCPUThreads);
    assert(perfCountList);
    for (unsigned int i = 0; i < NDRANGE_SIZE+nCPUThreads; i++)
      perfCountList[i].nFailedAttempts = 0;

#if nWorkGroups
    prepareGPUTest(context, actions, newValues, poppedNodes);

    // ===================== set up GPU arguments  =====================
    cl_int err = 0;
    err = clSetKernelArgSVMPointer(kernel, 0, (void*)memPool);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 1, (void*)queue);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 2, (void*)actions);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 3, (void*)newValues);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 4, (void*)poppedNodes);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 5, (void*)poolOverflow);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 6, (void*)counterOverflow);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 7, (void*)gateOpened);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 8, (void*)perfCountList);
    checkOCLError(err);
#endif

    // ===================== test code =====================
    /*
     * given a number of threads, each thread randomly either enqueues
     * a number to the queue or dequeues an element from the queue
     */

    /*
     * all enqueue & dequeue are tracked to verify the correctness
     */

    // each cpu thread and gpu has a corresponding map(Data --> count)
    std::unordered_map<Task, int> maps[nWorkUnits];

    // timers
    tHighResTimer totalTimer;

#if nWorkGroups
    // start GPU kernel
    std::cout << "Launching GPU kernel ... " << std::endl;
    err = clEnqueueNDRangeKernel(commandQueue, kernel, 1, 0, &globalSize, &wgsize, 0, NULL, &event);
    checkOCLError(err);

    // flush kernel command
    err = clFlush(commandQueue);
    checkOCLError(err);
#endif

    // start CPU threads
    std::vector<std::thread* > threads;

    tHighResTimer cpuTimer;

#if nWorkGroups
    while (!gateOpened->load(std::memory_order_acquire))          // wait until GPU thread
      ;
#endif

    for (size_t tid = 0u; tid < nCPUThreads; ++tid){
      threads.push_back(new std::thread(test, tid, std::ref(maps[nWorkGroups+tid]), std::ref(context),
                                          memPool, queue, perfCountList+NDRANGE_SIZE+tid));
    }

    // synchronize all threads
    for (auto &thread: threads){
      thread->join();
    }

//    std::cout << "CPU threads completed in " << cpuTimer.GetDuration()*1e3 << " ms" << std::endl;
    outputFile << cpuTimer.GetDuration()*1e3 << ", ";

#if nWorkGroups
    // wait for GPU kernel
    err = clFinish(commandQueue);
    checkOCLError(err);
#endif

//    std::cout << "Total completion time " << totalTimer.GetDuration()*1e3 << " ms" << std::endl;
    outputFile << totalTimer.GetDuration()*1e3 << ", ";

#if nWorkGroups
    cl_ulong s, e;
    if (clWaitForEvents(1 , &event) == CL_SUCCESS){ // wait for kernel execution
      err = clGetEventProfilingInfo(event,CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &s, NULL);
      err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &e, NULL);
    }

//    std::cout << "GPU threads completed in " << static_cast<long long>(e - s)/1e6 << " ms" << std::endl;
    outputFile << static_cast<long long>(e - s)/1e6 << "\n";
#endif

    // check if overflow happened?
    if (poolOverflow[0]){
      std::cout << std::flush;
      std::cerr << "GPU run out of memory" << std::endl;
    } else if (counterOverflow[0]){
      std::cout << std::flush;
      std::cerr << "ABA counter overflow" << std::endl;
    } else {
#if nWorkGroups
      // process GPU outputs
      processGPUOutput(actions, newValues, poppedNodes, maps);
#endif

      // drain all remaining elements from the queue
      std::cout << "Draining queue ... " << std::endl;
      while (true) {
        Task returnData;
        // dequeue a Data from queue
        dequeue(context, memPool, queue, &returnData, 0, perfCountList);
        if (returnData == INVALID_DATA) break;

  //    	std::cout << "Drained " << returnData << std::endl;
        maps[0][returnData]--;
      }

      // merge all unordered maps together to maps[0]
      std::unordered_map<Task, int> masterMap;

      for (unsigned int i = 0; i < nWorkUnits; ++i){
        // go through each entry in maps[i]
        for (auto &entry : maps[i])
          masterMap[entry.first] += entry.second;
      }

      // check if values of all entries in maps[0] are 0
      bool flag = true;
      for (auto &entry : masterMap){
        if (entry.second != 0){
          std::cout << "Data " << entry.first << " still has "
                              << entry.second << " copies in the queue" << std::endl;
          flag = false;
        }
      }

      if (!flag)
        std::cout << "Failed!" << std::endl;
      else
        std::cout << "Passed!" << std::endl;

  //    // dump statistics
  //    unsigned int nGPUFailures = 0;
  //    unsigned int nCPUFailures = 0;
  //
  //    double avgGPUFailures = 0.0;
  //    double avgCPUFailures = 0.0;
  //
  //    double stdDevGPUFailures = 0.0;
  //    double stdDevCPUFailures = 0.0;
  //
  //    // average
  //    for (unsigned int i = 0; i < globalSize; ++i)
  //      nGPUFailures += perfCountList[i].nFailedAttempts;
  //    avgGPUFailures = nGPUFailures/globalSize;
  //
  //    for (unsigned int i = globalSize; i < globalSize+nCPUThreads; ++i)
  //      nCPUFailures += perfCountList[i].nFailedAttempts;
  //    avgCPUFailures = nCPUFailures/(nCPUThreads*N_CPU_REPEAT);
  //
  //    std::cout << "avgGPUFailures " << avgGPUFailures << std::endl;
  //    std::cout << "avgCPUFailures " << avgCPUFailures << std::endl;
  //
  //    // standard deviation
  //    for (unsigned int i = 0; i < globalSize; ++i)
  //      stdDevGPUFailures += pow((perfCountList[i].nFailedAttempts - avgGPUFailures),2);
  //    stdDevGPUFailures = sqrt(stdDevGPUFailures/globalSize);
  //
  //    for (unsigned int i = globalSize; i < globalSize+nCPUThreads; ++i)
  //      stdDevCPUFailures += pow((perfCountList[i].nFailedAttempts/N_CPU_REPEAT - avgCPUFailures),2)*N_CPU_REPEAT;
  //    stdDevCPUFailures = sqrt(stdDevCPUFailures/(nCPUThreads*N_CPU_REPEAT));
  //
  //    std::cout << "stdDevGPUFailures " << stdDevGPUFailures << std::endl;
  //    std::cout << "stdDevCPUFailures " << stdDevCPUFailures << std::endl;

  //    for (unsigned int i = 0; i < globalSize+nCPUThreads; ++i)
  //      std::cout << "i = " << i << ": " << perfCountList[i].nFailedAttempts << std::endl;
    }

    // ===================== free opencl objects =====================
    // deallocate queue
    deallocateQueue(context, memPool, queue);

    // deallocate memPool
    deallocateFreePool(context, memPool);

#if nWorkGroups
    clSVMFree(context, actions);
    clSVMFree(context, poppedNodes);
    clSVMFree(context, newValues);
#endif

    clSVMFree(context, poolOverflow);
    clSVMFree(context, counterOverflow);
    clSVMFree(context, gateOpened);
    clSVMFree(context, perfCountList);
  }

  freeCLObjects(commandQueue, context, program, kernel);

  outputFile.close();
  return 0;
}

void test(int tid, std::unordered_map<Task, int> &myMap, cl_context &context, FreePool* memPool,
              LockFreeQueue* queue, PerfCount* perfCount){
	for (unsigned int repeat = 0; repeat < nTasksPerUnit; ++repeat){
		// generate random actions: enqueue & dequeue
		unsigned int action = rand() % 2;

		if (action == 0){	// enqueue
			// generate a random number to be enqueued
			Task newData = rand() % MAX;

      // create & initialize a new node
      Node* newNode = (Node*) allocateNode(context, memPool);
      assert(newNode);

      newNode->value = newData;
      newNode->nextPtr = (Pointer)nullptr;

			// enqueue newData to queue
			if (!enqueue(context, queue, newNode, tid, perfCount)){
			  std::cerr << "Overflow happened in enqueue tid = " << tid << std::endl;
			  return;
			}

//			std::cout << "Thread " << tid << " inserted " << newData << std::endl;

			// record the number in myMap
			myMap[newData]++;
		} else {					// dequeue
		  Task returnData;

			// dequeue a Data from queue
			if (!dequeue(context, memPool, queue, &returnData, tid, perfCount)){
        std::cerr << "Overflow happened in dequeue tid = " << tid << std::endl;
        return;
			}

			if (returnData != INVALID_DATA){
//	      std::cout << "Thread " << tid << " popped " << returnData << std::endl;
				myMap[returnData]--;
			} else {
//        std::cout << "Thread " << tid << " failed to pop from an empty queue" << std::endl;
			}
		}
	}
}

void prepareGPUTest(const cl_context &context, int* actions, int* newValues, int* poppedNodes){
  for (unsigned int i = 0; i < nGPUTasks; i++){
    newValues[i] = rand() % MAX;
    actions[i] = rand() % 2;
    poppedNodes[i] = INIT_NUMBER;
  }
}

void processGPUOutput(int* actions, int* newValues, int* poppedNodes, std::unordered_map<Task, int>* maps){
  std::cout << "Processing GPU output ... " << std::endl;

  for (unsigned int i = 0; i < nGPUTasks; i++){
    if (actions[i] == 0) {
//      std::cout << "GPU thread " << i << " enqueued - action = " << actions[i]
//                << " - value = " << newValues[i] << std::endl;
      maps[i/nTasksPerUnit][newValues[i]]++;
    } else {
//      std::cout << "GPU thread " << i << " dequeued - action = " << actions[i]
//                << " - popped value = " << poppedNodes[i] << std::endl;
      if (poppedNodes[i] != INVALID_DATA)
        maps[i/nTasksPerUnit][poppedNodes[i]]--;
    }
  }
}
