/*
 * List of macros shared by both host and kernel code
 */
#define INVALID_DATA -9999999
#define MAX_FREE_SPACE 1000000   // maximum number of free nodes
#define MAX 100000               // range of random values
#define INIT_NUMBER -9999998     // initial value of all elements in poppedValues array

#define ENQUEUE 0
#define DEQUEUE 1

#define LOCKED 1
#define UNLOCKED 0

#define MAX_ABA_COUNTER 65535   // the maximum unsigned integer that 16 bits can represent

#define DETECT_ABA_OVERFLOW 0   // 1: to detect overflow of aba counter

/*
 * A given number of tasks are statically distributed to all work units
 * (i.e., GPU work-group and CPU thread)
 * In this micro-test, a task is simply how many times a unit does enqueue or dequeue
 */
#define nWorkGroups 0                               // number of GPU work-groups
#define nCPUThreads 1                               // number of CPU threads
#define nWorkUnits (nWorkGroups + nCPUThreads)      // total number of work units available to work

#define WG_SIZE 256
#define NDRANGE_SIZE (WG_SIZE * nWorkGroups)

#define nTasks 1474560                              // divisible by the number of work-units (i.e., 1,2,4,6,8,10,12 and 256)
#define nTasksPerUnit (nTasks/nWorkUnits)
#define nTasksPerGPUThread (nTasksPerUnit/WG_SIZE)

#define nGPUTasks nTasksPerUnit*nWorkGroups
#define nCPUTasks nTasksPerUnit*nCPUThreads
