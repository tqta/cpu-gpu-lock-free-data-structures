#include "LockFreeQueue.hpp"
#include "FreeMemPool.hpp"

LockFreeQueue* constructQueue(const cl_context &context, FreePool* memPool){
  LockFreeQueue* queue = (LockFreeQueue*) allocateSVM<LockFreeQueue*>(context, 1);

  assert(queue);

  // create an initial dummy node
  Node* initNode = (Node*) allocateNode(context, memPool);
  assert(initNode);

  initNode->value = INVALID_DATA;

  // initially both head and tail pointing to this node with counter == 0
  queue->headPtr.store(getTaggedPointer(initNode, 0), std::memory_order_relaxed);
  queue->tailPtr.store(getTaggedPointer(initNode, 0), std::memory_order_relaxed);

  return queue;
}

bool enqueue(const cl_context &context, LockFreeQueue* queue, Node* newNode, int tid, PerfCount* perfCount){
  bool done = false;
  bool counterOverflow = false;                                           // detect if abaCounter overflows

  Pointer myTailPtr = (Pointer) nullptr;
  Node* myTailNode = nullptr;
  Pointer nextNode = (Pointer) nullptr;
  unsigned int abaCounter = 0;

  do {
    myTailPtr = queue->tailPtr.load(std::memory_order_acquire);           // snap global tailPtr & save it to myTailPtr
    myTailNode = getAddressFromTaggedPtr(myTailPtr);                      // extract the tail node's address
    abaCounter = getCounterFromTaggedPtr(myTailPtr);                      // retrieve ABA counter
    nextNode = myTailNode->nextPtr.load(std::memory_order_acquire);       // retrieve pointer to the node following my tail node

//    std::cout << "Thread " << tid << " is enqueuing: tailCounter = " << abaCounter << std::endl;

#if DETECT_ABA_OVERFLOW
    if (abaCounter >= MAX_ABA_COUNTER){
      counterOverflow = true;
    } else {
#endif
      if (myTailPtr == queue->tailPtr.load(std::memory_order_acquire)){     // are myTailPtr and global tailPtr consistent?
        if ((Node*)nextNode == nullptr){                                           // was myTail pointing to the last node whose next is Null?
          // if so, try to link the newNode to the global tail node
          if (myTailNode->nextPtr.compare_exchange_weak(nextNode, (Pointer)newNode, std::memory_order_seq_cst, std::memory_order_relaxed))
            done = true;
          else
            perfCount->nFailedAttempts++;
        } else {
          // other thread(s) already added a node and updated myTailNode
          // try to swing tail to the next node if the global tailPotr is still pointing to the old tail node
#if DETECT_ABA_OVERFLOW
          queue->tailPtr.compare_exchange_weak(myTailPtr, getTaggedPointer((Node*)nextNode, abaCounter+1u),
                                                std::memory_order_seq_cst, std::memory_order_relaxed);
#else
          queue->tailPtr.compare_exchange_weak(myTailPtr, getTaggedPointer((Node*)nextNode, (abaCounter+1u) % MAX_ABA_COUNTER),
                                                std::memory_order_seq_cst, std::memory_order_relaxed);
#endif
          perfCount->nFailedAttempts++;
        }
      }
#if DETECT_ABA_OVERFLOW
    }
#endif
  } while (!done && !counterOverflow);

  if (!counterOverflow) {
    // insertion was successful
    // try to swing tail to the inserted node
#if DETECT_ABA_OVERFLOW
    queue->tailPtr.compare_exchange_weak(myTailPtr, getTaggedPointer(newNode, abaCounter+1u),
                                          std::memory_order_seq_cst, std::memory_order_relaxed);
#else
    queue->tailPtr.compare_exchange_weak(myTailPtr, getTaggedPointer(newNode, (abaCounter+1u) % MAX_ABA_COUNTER),
                                          std::memory_order_seq_cst, std::memory_order_relaxed);
#endif
    return true;
  } else {
    return false;
  }
}

bool dequeue(const cl_context &context, FreePool* memPool, LockFreeQueue* queue, Task* returnData, int tid, PerfCount* perfCount){
  bool done = false;
  bool counterOverflow = false;                                           // detect if abaCounter overflows

  Pointer myHeadPtr = (Pointer) nullptr;
  Pointer myTailPtr = (Pointer) nullptr;
  Node* myTailNode = nullptr;
  Node* myHeadNode = nullptr;

  Pointer nextNode = (Pointer) nullptr;                                   // node following the head node

  unsigned int headABACounter = 0;
  unsigned int tailABACounter = 0;

  do {
    myHeadPtr = queue->headPtr.load(std::memory_order_acquire);           // snap global headPtr & save it to myHeadPtr
    myTailPtr = queue->tailPtr.load(std::memory_order_acquire);           // snap global tailPtr & save it to myTailPtr

    myHeadNode = getAddressFromTaggedPtr(myHeadPtr);                      // extract the head node's address
    myTailNode = getAddressFromTaggedPtr(myTailPtr);                      // extract the tail node's address

    headABACounter = getCounterFromTaggedPtr(myHeadPtr);                  // retrieve ABA counter of the head
    tailABACounter = getCounterFromTaggedPtr(myTailPtr);                  // retrieve ABA counter of the tail

//    std::cout << "Thread " << tid << " is dequeuing: headCounter = " << headABACounter
//                  << " tailCounter = " << tailABACounter << std::endl;

    nextNode = myHeadNode->nextPtr.load(std::memory_order_acquire);       // retrieve pointer to the node following my head node

#if DETECT_ABA_OVERFLOW
    if (headABACounter >= MAX_ABA_COUNTER || tailABACounter >= MAX_ABA_COUNTER){
      counterOverflow = true;
    } else {
#endif
      if (myHeadPtr == queue->headPtr.load(std::memory_order_acquire)){   // are myHeadPtr and global headPtr consistent
        if (myHeadNode == myTailNode){                                    // is queue empty or tail falling behind
          if (nextNode == (Pointer) nullptr){                             // is queue empty
            *returnData = INVALID_DATA;
            done = true;                                                  // queue is empty & returnData is INVALID
          } else {
            // tail is falling behind -> try to advance the tail
#if DETECT_ABA_OVERFLOW
            queue->tailPtr.compare_exchange_weak(myTailPtr, getTaggedPointer((Node*)nextNode, tailABACounter+1u),
                                                  std::memory_order_seq_cst, std::memory_order_relaxed);
#else
            queue->tailPtr.compare_exchange_weak(myTailPtr, getTaggedPointer((Node*)nextNode, (tailABACounter+1u) % MAX_ABA_COUNTER),
                                                  std::memory_order_seq_cst, std::memory_order_relaxed);
#endif
            perfCount->nFailedAttempts++;
          }
        } else {
          // read value before CAS, otherwise another dequeue might free the next node
          *returnData = ((Node*)nextNode)->value;

          // try to swing head to the next node
#if DETECT_ABA_OVERFLOW
          if (queue->headPtr.compare_exchange_weak(myHeadPtr, getTaggedPointer((Node*)nextNode, headABACounter+1u),
                                                    std::memory_order_seq_cst, std::memory_order_relaxed))
#else
          if (queue->headPtr.compare_exchange_weak(myHeadPtr, getTaggedPointer((Node*)nextNode, (headABACounter+1u) % MAX_ABA_COUNTER),
                                                    std::memory_order_seq_cst, std::memory_order_relaxed))
#endif
            done = true;
          else
            perfCount->nFailedAttempts++;
        }
      }
#if DETECT_ABA_OVERFLOW
    }
#endif
  } while (!done && !counterOverflow);

  if (!counterOverflow) {
    if (myHeadNode && (Node*)nextNode) deallocateNode(context, myHeadNode, memPool);
    return true;
  } else {
    return false;
  }
}

void deallocateQueue(const cl_context &context, FreePool* memPool, LockFreeQueue* queue){
  assert(memPool);
  assert(queue);

  // even the queue is empty, there is at least one dummy node in the queue
  assert(queue->headPtr);

  deallocateNode(context, getAddressFromTaggedPtr(queue->headPtr.load(std::memory_order_acquire)), memPool);

  queue->headPtr.store(0);
  queue->tailPtr.store(0);

  clSVMFree(context, queue);
}
