#ifndef STRUCTURE_HPP
#define STRUCTURE_HPP

#include "SharedMacros.h"

#include <atomic>
#include <thread>

/*
 * Data could be separate structure
 * Use "int" type for simplicity
 */
typedef int Task;

/*
 * Define custom pointer types
 */
typedef std::atomic<unsigned long> AtomicPointer;
typedef unsigned long Pointer;

/*
 *  A node in a linked list is a pair of value
 *  and pointer to the next node
 */
typedef struct NodeStruct {
  AtomicPointer nextPtr;
  Task value;
} Node;

/*
 * a queue is basically a linked list
 * that has both head and tail pointers
 * and corresponding lock variables
 *
 * headPtr & tailPtr are tagged
 * the low 16 bits represent an associated counter to avoid ABA problem
 * the high 48 bits are real address
 */
typedef struct {
  AtomicPointer headPtr; // pointer to head
  AtomicPointer tailPtr; // pointer to tail
} LockFreeQueue;

/*
 * a free memory pool has a root pointing to the first slot
 * and nextFreeSlot is the index of the next available slot
 */
typedef struct {
  Node* root;                     // root pointer to the pool
  std::atomic_uint* nextFreeSlot; // index of the next free slot
} FreePool;

/*
 * Kernel performance counters
 */
typedef struct {
  unsigned int nFailedAttempts;
} PerfCount;

#endif
