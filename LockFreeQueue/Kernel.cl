#include "SharedMacros.h"

#define true 1
#define false 0

#define order_acq memory_order_acquire
#define order_rel memory_order_release
#define order_acq_rel memory_order_acq_rel
#define order_seq_cst memory_order_seq_cst
#define order_relaxed memory_order_relaxed

#define scope_svm memory_scope_all_svm_devices

#define atomicLoad atomic_load_explicit
#define atomicStore atomic_store_explicit
#define atomicCAS atomic_compare_exchange_weak_explicit
#define atomicAdd atomic_fetch_add_explicit

/*************************************
 * Performance counters
 *************************************/
typedef struct {
  unsigned int nFailedAttempts; 
} PerfCount; 

/*************************************
 * OpenCL version of Structures.hpp
 *************************************/

typedef int Data;

/*
 * Define custom pointer types
 */
typedef atomic_ulong AtomicPointer;
typedef unsigned long Pointer;

/*
 *  A node in a linked list is a pair of value
 *  and pointer to the next node
 */
typedef struct NodeStruct {
  AtomicPointer nextPtr;
  Data value;
} Node;

/*
 * a queue is basically a linked list
 * that has both head and tail pointers
 * and corresponding lock variables
 *
 * headPtr & tailPtr are tagged
 * the low 16 bits represent an associated counter to avoid ABA problem
 * the high 48 bits are real address
 */
typedef struct {
  AtomicPointer headPtr; // pointer to head
  AtomicPointer tailPtr; // pointer to tail
} LockFreeQueue;

/*
 * a free memory pool has a root pointing to the first slot
 * and nextFreeSlot is the index of the next available slot
 */
typedef struct {
  __global Node* root;                         // root pointer to the pool
  __global volatile atomic_uint* nextFreeSlot; // index of the next free slot
} FreePool;

/*************************************
 * OpenCL version of FreeMemPool.hpp
 *************************************/

/*
 * return pointer to the next free node
 */
__global Node* allocateNode(__global volatile FreePool* pool){
  // do an atomic increment on nextFreeSlot
  unsigned int mySlot = atomicAdd(pool->nextFreeSlot, 1, order_acq_rel, scope_svm);

  if (mySlot < MAX_FREE_SPACE){
    // my slot has been reserved successfully
    return (pool->root + mySlot);
  } else {
    return NULL;
  }
}

/*
 * deallocate a node: putting back a node to free pool
 */
void deallocateNode(__global Node* node, __global volatile FreePool* pool){
  /*
   *  TODO this function is not complete now
   *  putting back node to free pool requires a more complex structure like concurrent queue
   */

  node = NULL;
}

/*************************************
 * OpenCL version of LockFreeQueue.hpp
 *************************************/

/*
 * Combine a 64-bit node pointer and unsigned int counter into a TaggedPointer
 * Assume: caller already checks boundary condition of counter
 */
Pointer getTaggedPointer(__global Node* address, unsigned int counter){
  return ((Pointer) address << 16) | (Pointer) counter;
};

/*
 * Extract address field of a tagged pointer
 */
__global Node* getAddressFromTaggedPtr(Pointer taggedPtr){
  return (__global Node*)(taggedPtr >> 16);
};

/*
 * Extract counter field of a tagged pointer
 */
unsigned int getCounterFromTaggedPtr(Pointer taggedPtr){
  return (unsigned int)(taggedPtr & 0x000000000000FFFF);
};

bool enqueue(__global volatile LockFreeQueue* queue, __global Node* newNode, __global PerfCount* perfCount){
  bool done = false;
  bool counterOverflow = false;                                           // detect if abaCounter overflows

  Pointer myTailPtr = (Pointer) NULL;
  __global Node* myTailNode = NULL;
  Pointer nextNode = (Pointer) NULL;
  unsigned int abaCounter = 0;

  do {
    myTailPtr = atomicLoad(&(queue->tailPtr), order_acq, scope_svm);      // snap global tailPtr & save it to myTailPtr
    myTailNode = getAddressFromTaggedPtr(myTailPtr);                      // extract the tail node's address
    abaCounter = getCounterFromTaggedPtr(myTailPtr);                      // retrieve ABA counter
    nextNode = atomicLoad(&(myTailNode->nextPtr), order_acq, scope_svm);  // retrieve pointer to the node following my tail node

#if DETECT_ABA_OVERFLOW
    if (abaCounter >= MAX_ABA_COUNTER){
      counterOverflow = true;
    } else {
#endif
      if (myTailPtr == atomicLoad(&(queue->tailPtr), order_acq, scope_svm)){     // are myTailPtr and global tailPtr consistent?
        if ((__global Node*) nextNode == NULL){                                  // was myTail pointing to the last node whose next is Null?
          // if so, try to link the newNode to the global tail node
          if (atomicCAS(&(myTailNode->nextPtr), &nextNode, (Pointer)newNode, order_seq_cst, order_relaxed, scope_svm))
            done = true;
          else 
            perfCount->nFailedAttempts++;                                        // this is a failed attempt

        } else {
          // other thread(s) already added a node and updated myTailNode
          // try to swing tail to the next node if the global tailPotr is still pointing to the old tail node
#if DETECT_ABA_OVERFLOW
          atomicCAS(&(queue->tailPtr), &myTailPtr, getTaggedPointer((__global Node*)nextNode, abaCounter+1), 
                      order_seq_cst, order_relaxed, scope_svm);
#else
          atomicCAS(&(queue->tailPtr), &myTailPtr, getTaggedPointer((__global Node*)nextNode, (abaCounter+1) % MAX_ABA_COUNTER), 
                      order_seq_cst, order_relaxed, scope_svm);
#endif
          perfCount->nFailedAttempts++;                                          // this is a failed attempt
        }
      }
#if DETECT_ABA_OVERFLOW
    }
#endif
  } while (!done && !counterOverflow);

  if (!counterOverflow) {
    // insertion was successful
    // try to swing tail to the inserted node
#if DETECT_ABA_OVERFLOW
    atomicCAS(&(queue->tailPtr), &myTailPtr, getTaggedPointer(newNode, abaCounter+1), 
                order_seq_cst, order_relaxed, scope_svm);
#else
    atomicCAS(&(queue->tailPtr), &myTailPtr, getTaggedPointer(newNode, (abaCounter+1) % MAX_ABA_COUNTER), 
                order_seq_cst, order_relaxed, scope_svm);
#endif
    return true;
  } else {
    return false;
  }
}

bool dequeue(__global volatile FreePool* memPool, __global volatile LockFreeQueue* queue, 
              __global Data* returnData, __global PerfCount* perfCount){
  bool done = false;
  bool counterOverflow = false;                                           // detect if abaCounter overflows

  Pointer myHeadPtr = (Pointer) NULL;
  Pointer myTailPtr = (Pointer) NULL;
  __global Node* myTailNode = NULL;
  __global Node* myHeadNode = NULL;

  Pointer nextNode = (Pointer) NULL;                                      // node following the head node

  unsigned int headABACounter = 0;
  unsigned int tailABACounter = 0;

  do {
    myHeadPtr = atomicLoad(&(queue->headPtr), order_acq, scope_svm);      // snap global headPtr & save it to myHeadPtr
    myTailPtr = atomicLoad(&(queue->tailPtr), order_acq, scope_svm);      // snap global tailPtr & save it to myTailPtr

    myHeadNode = getAddressFromTaggedPtr(myHeadPtr);                      // extract the head node's address
    myTailNode = getAddressFromTaggedPtr(myTailPtr);                      // extract the tail node's address

    headABACounter = getCounterFromTaggedPtr(myHeadPtr);                  // retrieve ABA counter of the head
    tailABACounter = getCounterFromTaggedPtr(myTailPtr);                  // retrieve ABA counter of the tail

    nextNode = atomicLoad(&(myHeadNode->nextPtr), order_acq, scope_svm);  // retrieve pointer to the node following my head node

#if DETECT_ABA_OVERFLOW
    if (headABACounter >= MAX_ABA_COUNTER || tailABACounter >= MAX_ABA_COUNTER){
      counterOverflow = true;
    } else {
#endif
      if (myHeadPtr == atomicLoad(&(queue->headPtr), order_acq, scope_svm)){   // are myHeadPtr and global headPtr consistent
        if (myHeadNode == myTailNode){                                         // is queue empty or tail falling behind
          if (nextNode == (Pointer) NULL){                                     // is queue empty
            *returnData = INVALID_DATA;
            done = true;                                                       // queue is empty & returnData is INVALID
          } else {
            // tail is falling behind -> try to advance the tail
#if DETECT_ABA_OVERFLOW
            atomicCAS(&(queue->tailPtr), &myTailPtr, getTaggedPointer((__global Node*)nextNode, tailABACounter+1),
                        order_seq_cst, order_relaxed, scope_svm);
#else
            atomicCAS(&(queue->tailPtr), &myTailPtr, getTaggedPointer((__global Node*)nextNode, (tailABACounter+1) % MAX_ABA_COUNTER),
                        order_seq_cst, order_relaxed, scope_svm);
#endif
            perfCount->nFailedAttempts++;                                      // this is a failed attempt
          }
        } else {
          // read value before CAS, otherwise another dequeue might free the next node
          *returnData = ((Node*)nextNode)->value;

          // try to swing head to the next node
#if DETECT_ABA_OVERFLOW
          if (atomicCAS(&(queue->headPtr), &myHeadPtr, getTaggedPointer((__global Node*)nextNode, headABACounter+1),
                          order_seq_cst, order_relaxed, scope_svm))
#else
          if (atomicCAS(&(queue->headPtr), &myHeadPtr, getTaggedPointer((__global Node*)nextNode, (headABACounter+1) % MAX_ABA_COUNTER),
                          order_seq_cst, order_relaxed, scope_svm))
#endif
            done = true;
          else 
            perfCount->nFailedAttempts++;                                       // this is a failed attempt
        }
      }
#if DETECT_ABA_OVERFLOW
    }
#endif
  } while (!done && !counterOverflow);

  if (!counterOverflow) {
    if (myHeadNode && (__global Node*)nextNode) deallocateNode(myHeadNode, memPool);
    return true;
  } else {
    return false;
  }
}

/* ========================= Main kernel ================================== */
/*
 *  each thread calls either enqueue or dequeue function
 */
__kernel void concurrentQueue(__global volatile FreePool* memPool,
                              __global volatile LockFreeQueue* queue,   
                              __global const int* actions,    
                              __global const int* newValues,              // values to be pushed to the queue
                              __global int* poppedNodes,                  // list of numbers popped from the queue
                              __global int* poolOverflow,                 // simple flag showing if we're running out of free space
                              __global int* counterOverflow,              // simple flag showing if our ABA counter overflows
                              __global atomic_int* gateOpened,            // flag showing whether GPU threads already started
                              __global PerfCount* perfCount               // performance counters  
){
  int globalThreadID = get_global_id(0);
  int localThreadID = get_local_id(0);
  int workUnitID = get_group_id(0);

  if (globalThreadID == 0) atomicStore(gateOpened, true, order_rel, scope_svm);        // signal CPU

  for (int i = 0; i < nTasksPerGPUThread; i++){
    int myIndex = (workUnitID*nTasksPerUnit) + WG_SIZE*i + localThreadID;

    int myAction = actions[myIndex];

    if (myAction == ENQUEUE){  // call enqueue
      __global Node* newNode = allocateNode(memPool);   // make a new node
      if (!newNode) {
        // we can't do anything other than giving up GPU resource
        poolOverflow[0] = true;
        return;     
      }

      newNode->value = newValues[myIndex];
      atomicStore(&(newNode->nextPtr), (Pointer)NULL, order_rel, scope_svm);

      if (!enqueue(queue, newNode, perfCount+globalThreadID)) 
        counterOverflow[0] = true;                    
    } else {                  // call dequeue
      if (!dequeue(memPool, queue, poppedNodes+myIndex, perfCount+globalThreadID)) 
        counterOverflow[0] = true;  
    }
  }
  return;
}
