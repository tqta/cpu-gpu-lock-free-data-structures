#ifndef DEVICE_SETUP
#define DEVICE_SETUP

#include <CL/opencl.h>
#include <stdlib.h>
#include <stdio.h>

void createPlatform(cl_platform_id* platform, int platformID);

void createContext(cl_context* context, cl_platform_id platform, cl_device_id* devices, int deviceID);

void createCommandQueue(cl_context context, cl_device_id device, cl_command_queue* commandQueue);

void compileKernel(const char* kernelFileName, const char* kernelName, 
					cl_context context, cl_program* program, cl_device_id* device,
					cl_kernel* kernel);

cl_mem createBuffer(cl_context context, cl_mem_flags flags, size_t buffSize);

void writeToDevice(cl_command_queue commandQueue, cl_mem buffer, void* host_ptr, size_t buffSize);
void readFromDevice(cl_command_queue commandQueue, cl_mem buffer, void* host_ptr, size_t buffSize);

void* mapBuffer(cl_command_queue commandQueue, cl_mem buffer, cl_map_flags flags, size_t buffSize);

void unmapBuffer(cl_command_queue commandQueue, cl_mem buffer, void* ptr);

void commandQueueSync(cl_command_queue commandQueue);

char* readKernelSource(const char *fileName);

const char* checkError(cl_int err);

#endif
