/*
 *	Implementation of work-stealing algorithm on GPU
 *	Each wavefront has its own work-pool stored in global memory
 *	Work-pool is a double-ended queue (DEqueue)
 * 	Only local wavefront can pop/push from/to its own pool through its tail's pointer
 *	A wavefronts can steal tasks from others through their head's pointer
 */

#include "timer.hpp"
#include "deviceSetup.hpp"
#include <math.h>
#include <atomic>

#define SUCCESS 0
#define FAILURE 999999999

unsigned long combine(unsigned int num1, unsigned int num2){
	return (((unsigned long) num1) << 32) | ((unsigned long) num2);	
}

unsigned int getNum1(unsigned long combined){
	return (unsigned int) (combined >> 32);
}

unsigned int getNum2(unsigned long combined){
	unsigned long mask = 0x00000000FFFFFFFF;
	return (unsigned int) (mask & combined);
}

void h_vecAdd(int startIdx, int* A, int* B, int* C);
unsigned int pop(std::atomic<unsigned long>* head, std::atomic<unsigned int>* tail);
unsigned int steal(std::atomic<unsigned long>* head, std::atomic<unsigned int>* tail);

int main(){
	cl_context context;
	cl_command_queue commandQueue; 		
	cl_platform_id platform;			
	cl_uint deviceCount;
	cl_device_id device;		
	cl_program program;					
	cl_kernel kernel;	

	size_t kernel_size;					
	cl_event event; 					
	cl_int err;							

	// ================== Set up OpenCL components =====================
	int deviceID = 0;
	int platformID = 0;

	createPlatform(&platform, platformID);
	createContext(&context, platform, &device, deviceID);
	createCommandQueue(context, device, &commandQueue);
	compileKernel("kernel.cl", "work_stealing", context, &program, &device, &kernel);

	// =================================================================
  /*
      workPool - a set of task indices
               - chunked into numGroups segments
               - each group of threads own a segment

      queueID  - id of the working group of threads
               - on GPU, queueID is simply a WF_ID
               - CPU set its queueIDs separately

      heads    - list of head indices
               - heads[queueID] contains the index of my head in workPool

      tails    - list of tail indices
               - tails[queueID] is the index of my tail in workPool

      startWorkingIndex - once a group of threads is assigned some tasks
                        - this is the starting index of where the group should start from
                        - thread with laneID in a group will work on workPool[startWorkingIndex + laneID]

      numGroups - the total number of thread groups on both CPU and GPU
  */

  unsigned int numWGs = 8;
  unsigned int numCPUThreads = 0;
  unsigned int numGroups = numWGs + numCPUThreads; // GPU WGs + CPU threads

	size_t wgSize = 64; // set it to the WF size. TODO may generalize it in the future
	size_t globalSize = wgSize * numWGs;

	unsigned int numTasksPerGroup = 25600;			// number of tasks per thread group
  unsigned int numTasks = numTasksPerGroup * numGroups;   // total number of tasks

	int* workPool = (int*) clSVMAlloc (context, 
                                     CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS, 
                                     sizeof(int) * numTasks,
                                     0);

  for (int i = 0; i < numTasks; i++) 
    workPool[i] = i;

	std::atomic<unsigned long>* heads = (std::atomic<unsigned long>*) clSVMAlloc (context, 
                                                            CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                                                            sizeof(unsigned long) * numGroups,
                                                            0);

	std::atomic<unsigned int>* tails = (std::atomic<unsigned int>*) clSVMAlloc (context, 
                                                            CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                                                            sizeof(unsigned int) * numGroups,
                                                            0);

  unsigned int* startWorkingIndex = (unsigned int*) clSVMAlloc (context, 
                                                                CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                                                                sizeof(unsigned long) * numGroups,
                                                                0);

  for (int i = 0; i < numGroups; i++) {
    heads[i] = combine(0,0);
    tails[i] = numTasksPerGroup;
    startWorkingIndex[i] = 0;   // to detect any runtime bug
  }

  // create vecAdd buffer
  int* A = (int*) clSVMAlloc(context, 
                             CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                             sizeof(int) * numTasks,
                             0);
  int* B = (int*) clSVMAlloc(context, 
                             CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                             sizeof(int) * numTasks,
                             0);
  int* C = (int*) clSVMAlloc(context, 
                             CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                             sizeof(int) * numTasks,
                             0);

  int* mark = (int*) clSVMAlloc(context, 
                             CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                             sizeof(int) * numTasks,
                             0);
  
  for (int i = 0; i < numTasks; i++) {
    A[i] = i;
    B[i] = i;
    C[i] = 0;
    mark[i] = 0;
  }

  // set kernel args
	err = clSetKernelArgSVMPointer(kernel, 0, (void*)workPool);
	err = clSetKernelArgSVMPointer(kernel, 1, (void*)heads);
	err = clSetKernelArgSVMPointer(kernel, 2, (void*)tails);
	err = clSetKernelArgSVMPointer(kernel, 3, (void*)startWorkingIndex);
	err = clSetKernelArg(kernel, 4, sizeof(cl_uint), (void*)&numGroups);
	err = clSetKernelArg(kernel, 5, sizeof(cl_uint), (void*)&numTasksPerGroup);
	err = clSetKernelArgSVMPointer(kernel, 6, (void*)A);
	err = clSetKernelArgSVMPointer(kernel, 7, (void*)B);
	err = clSetKernelArgSVMPointer(kernel, 8, (void*)C);
	err = clSetKernelArgSVMPointer(kernel, 9, (void*)mark);

	// Launch the kernel
	err = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, &globalSize, &wgSize, 0, NULL, NULL);

	if (err != CL_SUCCESS){
    printf("Error in clEnqueueNDRangeKernel: %s\n", checkError(err));
    exit(-1);
  }

  clFlush(commandQueue);

  // -------------- CPU executes this part --------
if (numCPUThreads > 0) {
  unsigned int queueID = numWGs;
  unsigned int myQueueStartIdx = queueID * numTasksPerGroup;

  while (true){
    // try to pop from CPU queue
    unsigned int startIdx = pop(heads+queueID, tails+queueID);

    // if there is no task from CPU's queue to do 
    if (startIdx == FAILURE) {
      // look into neighbor queue and try to steal task from it
      unsigned int victimQueueID = (queueID + 1) % numGroups;
      unsigned int victimQueueStartIdx = victimQueueID * numTasksPerGroup; 

      while (victimQueueID != queueID) {
        startIdx = steal(heads+victimQueueID, tails+victimQueueID);

        if (startIdx != FAILURE) {
          printf("CPU steals task from GPU queue %d %d\n", victimQueueID, startIdx);
          startWorkingIndex[queueID] = startIdx + victimQueueStartIdx;
          break;
        }

        // otherwise, try another queue
        victimQueueID = (victimQueueID + 1) % numGroups;
        victimQueueStartIdx = victimQueueID * numTasksPerGroup;
      }

      // I have looked at all available queue --> DONE
      if (victimQueueID == queueID)
        startWorkingIndex[queueID] = FAILURE;
    } else {
      printf("CPU pops task from its queue %d %d\n", queueID, startIdx);
      startWorkingIndex[queueID] = startIdx + myQueueStartIdx; 
    }

    startIdx = startWorkingIndex[queueID];
    unsigned int index;

    if (startIdx != FAILURE) {
      for (int laneID = 0; laneID < 64; laneID++){
        index = workPool[startIdx + laneID];
        C[index] = A[index] + B[index];
        mark[index] = queueID;
      }
    } else {
      break;
    }
	}
}
  // -------------------------------------------------

	commandQueueSync(commandQueue);

  // checking 
  int flag = 1;

  for (int i = 0; i < numTasks; i++) {
    if (A[i] + B[i] != C[i]) {
      printf("%d %d %d %d\n", i, A[i], B[i], C[i]);
      flag = 0;
    } 
    printf("%d %d\n", i, mark[i]);
  }

  if (flag == 1)
    printf("All outputs are correct\n");
  else 
    printf("Incorrect output\n");

  clSVMFree(context, workPool);
  clSVMFree(context, heads);  
  clSVMFree(context, tails);
  clSVMFree(context, A);
  clSVMFree(context, B);
  clSVMFree(context, C);

	// ------- Release OpenCL objects -------
	if(commandQueue)clReleaseCommandQueue(commandQueue);
	if(context)clReleaseContext(context);
	if(program)clReleaseProgram(program);
	if(kernel)clReleaseKernel(kernel);
	if(event)clReleaseEvent(event);

	return 0;
}

// Head = (index, counter)
// ========== pop 64 tasks per wavefront ==================
// only lane0 thread executes pop()
// return task index in the queue
unsigned int pop(std::atomic<unsigned long>* head, std::atomic<unsigned int>* tail){
  unsigned int localTail = atomic_load_explicit(tail, 
                                                std::memory_order_seq_cst);

	// queue is empty --> return failure
	if (localTail == 0) return FAILURE;

	// try to update the globalTail
  // NOTE localTail is always a multiple of 64 --> no worry about localTail < 0
	localTail -= 64;

	atomic_store_explicit(tail, localTail, std::memory_order_seq_cst);


  // check the globalHead
  unsigned long localHead = atomic_load_explicit(head, 
                                                 std::memory_order_seq_cst);

  // if localTail > global head index 
  // --> we're safe to work on 64 elements starting from the localTail
  // return the localTail as a start working index
  if (localTail > getNum1(localHead)) {
      return localTail; 
  }

  // if we reach here, then
  // either localTail == localHead 
  // or localTail < localHead
  
  // in both cases, set the globalTail to 0
  unsigned int new_val = 0;
  atomic_store_explicit(tail, new_val, std::memory_order_seq_cst);

  // prepare to set the globalHead to 0 also
  unsigned long newHead = combine(0, getNum2(localHead) + 1);		

  if (localTail == getNum1(localHead)) {
    // this could be the last block in the queue
    // I'll try to grab this block by CAS
    if (atomic_compare_exchange_strong_explicit(head, &localHead, newHead, 
                                                std::memory_order_seq_cst, std::memory_order_relaxed)) {
      // global head has not been changed,
      // and it has just been reset to 0
      // we can safely work on the task
      return localTail;
    }

    // otherwise, someone has already stolen the last block
  }

  // anyway, we should reset the head to 0
	atomic_store_explicit(head, 
                        newHead, 
                        std::memory_order_seq_cst);

  // and return FAILURE
	return FAILURE;
}
// ========================================================

// =========== steal 64 tasks from other queue ======
unsigned int steal(std::atomic<unsigned long>* head, std::atomic<unsigned int>* tail){
	unsigned long oldHead, newHead;
	unsigned int taskIndex;

	unsigned long localHead = atomic_load_explicit(head, 
                                                 std::memory_order_seq_cst);

  unsigned int localTail = atomic_load_explicit(tail, 
                                                std::memory_order_seq_cst);

	// victim queue has no task left
	if (localTail <= getNum1(localHead)) return FAILURE;

  // otherwise, I could steal at least one task block from the victim 

  // prepare what would be the newHead if I successfully steal a block
	newHead = combine(getNum1(localHead) + 64, getNum2(localHead) + 1);

	if (atomic_compare_exchange_strong_explicit(head, &localHead, newHead, 
                                              std::memory_order_seq_cst, std::memory_order_relaxed)) {
    // successfully stole the block
    // return the start index
	  return getNum1(localHead);
	}

  // otherwise, the victim queue is empty at the time of CAS --> give up
	return FAILURE;
}
