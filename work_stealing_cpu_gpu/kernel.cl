#pragma OPENCL EXTENSION cl_khr_int64_extended_atomics : enable
//#pragma OPENCL EXTENSION cl_amd_printf :enable

#define SUCCESS 0
#define FAILURE 999999999

#define WF_SIZE 64

#define TRUE 1
#define FALSE 0

// ==== combine 2 32-bit integers into 1 64-bit number ====
unsigned long combine(unsigned int num1, unsigned int num2){
	return (((unsigned long) num1) << 32) | ((unsigned long) num2);
}

unsigned int getNum1(unsigned long combined){
	return (unsigned int) (combined >> 32);
}

unsigned int getNum2(unsigned long combined){
	unsigned long mask = 0x00000000FFFFFFFF;
	return (unsigned int) (mask & combined);
}
// ====================WORKED==============================

// Head = (index, counter)
// ========== pop 64 tasks per wavefront ==================
// only lane0 thread executes pop()
// return task index in the queue
unsigned int pop(atomic_ulong* head, atomic_uint* tail){
  unsigned int localTail = atomic_load_explicit(tail, 
                                                memory_order_seq_cst, 
                                                memory_scope_all_svm_devices);

	// queue is empty --> return failure
	if (localTail == 0) return FAILURE;

	// try to update the globalTail
  // NOTE localTail is always a multiple of 64 --> no worry about localTail < 0
	localTail -= 64;

	atomic_store_explicit(tail, 
                        localTail, 
                        memory_order_seq_cst, 
                        memory_scope_all_svm_devices);


  // check the globalHead
  unsigned long localHead = atomic_load_explicit(head, 
                                                 memory_order_seq_cst, 
                                                 memory_scope_all_svm_devices);

  // if localTail > global head index 
  // --> we're safe to work on 64 elements starting from the localTail
  // return the localTail as a start working index
  if (localTail > getNum1(localHead)) {
      return localTail; 
  }

  // if we reach here, then
  // either localTail == localHead 
  // or localTail < localHead
  
  // in both cases, set the globalTail to 0
  atomic_store_explicit(tail, 
                        0, 
                        memory_order_seq_cst, 
                        memory_scope_all_svm_devices);

  // prepare to set the globalHead to 0 also
  unsigned long newHead = combine(0, getNum2(localHead) + 1);		

  if (localTail == getNum1(localHead)) {
    // this could be the last block in the queue
    // I'll try to grab this block by CAS
    if (atomic_compare_exchange_strong_explicit(head, &localHead, newHead, 
                                                memory_order_seq_cst, memory_order_relaxed, 
                                                memory_scope_all_svm_devices)) {
      // global head has not been changed,
      // and it has just been reset to 0
      // we can safely work on the task
      return localTail;
    }

    // otherwise, someone has already stolen the last block
  }

  // anyway, we should reset the head to 0
	atomic_store_explicit(head, 
                        newHead, 
                        memory_order_seq_cst, 
                        memory_scope_all_svm_devices);

  // and return FAILURE
	return FAILURE;
}
// ========================================================

// =========== steal 64 tasks from other queue ======
unsigned int steal(atomic_ulong* head, atomic_uint* tail){
	unsigned long oldHead, newHead;
	unsigned taskIndex;

	unsigned long localHead = atomic_load_explicit(head, 
                                                 memory_order_seq_cst, 
                                                 memory_scope_all_svm_devices);

  unsigned int localTail = atomic_load_explicit(tail, 
                                                memory_order_seq_cst, 
                                                memory_scope_all_svm_devices);

	// victim queue has no task left
	if (localTail <= getNum1(localHead)) return FAILURE;

  // otherwise, I could steal at least one task block from the victim 

  // prepare what would be the newHead if I successfully steal a block
	newHead = combine(getNum1(localHead) + 64, getNum2(localHead) + 1);

	if (atomic_compare_exchange_strong_explicit(head, &localHead, newHead, 
                                              memory_order_seq_cst, memory_order_relaxed, 
                                              memory_scope_all_svm_devices)) {
    // successfully stole the block
    // return the start index
	  return getNum1(localHead);
	}

  // otherwise, the victim queue is empty at the time of CAS --> give up
	return FAILURE;
}
// ========================================================

// ================= push 64 tasks into the queue =========
// TODO TBD - consider how to reserve 64 spots in a queue
//            and how to make them globally visible safely
// ========================================================


// ===================== Main kernel ========================
/*
    workPool - a set of task indices
             - chunked into numGroups segments
             - each group of threads own a segment

    queueID  - id of the working group of threads
             - on GPU, queueID is simply a WF_ID
             - CPU set its queueIDs separately

    heads    - list of head indices
             - heads[queueID] contains the index of my head in workPool

    tails    - list of tail indices
             - tails[queueID] is the index of my tail in workPool

    startWorkingIndex - once a group of threads is assigned some tasks
                      - this is the starting index of where the group should start from
                      - thread with laneID in a group will work on workPool[startWorkingIndex + laneID]

    numGroups - the total number of thread groups on both CPU and GPU
*/

__kernel void work_stealing(
              global int* workPool,			
							global atomic_ulong* heads,			
							global atomic_uint* tails,			
              global unsigned int* startWorkingIndex,
              const unsigned int numGroups,
              const unsigned int numTasksPerGroup,

              global const int* A,
              global const int* B,
              global int* C,
              global int* mark
){
	// get lane ID
	unsigned int laneID = get_local_id(0) % WF_SIZE;

	// get queue_id
	unsigned int queueID = get_global_id(0) / WF_SIZE;

  unsigned int myQueueStartIdx = queueID * numTasksPerGroup;

  // loop until no job is available
	while (true){
		// laneID 0 thread goes and gets 64 tasks at a time from its own queue
		if (laneID == 0) {
      unsigned int startIdx = pop(heads+queueID, tails+queueID);

      // if there is no task to do 
      if (startIdx == FAILURE) {
        unsigned int victimQueueID = (queueID + 1) % numGroups;
        unsigned int victimQueueStartIdx = victimQueueID * numTasksPerGroup; 

        while (victimQueueID != queueID) {
          startIdx = steal(heads+victimQueueID, tails+victimQueueID);

          if (startIdx != FAILURE) {
            startWorkingIndex[queueID] = startIdx + victimQueueStartIdx;
            break;
          }

          // otherwise, try to steal from another queue
          victimQueueID = (victimQueueID + 1) % numGroups;
          victimQueueStartIdx = victimQueueID * numTasksPerGroup;
        }

        // I have looked at all available queue --> DONE
        if (victimQueueID == queueID)
          startWorkingIndex[queueID] = FAILURE;
      } else {
        startWorkingIndex[queueID] = startIdx + myQueueStartIdx; 
      }
    }

    mem_fence(CLK_GLOBAL_MEM_FENCE);

    // all 64 threads in a WF start working together from here
    unsigned int startIdx = startWorkingIndex[queueID];

    if (startIdx != FAILURE) {
      unsigned int index = workPool[startIdx + laneID];
      C[index] = A[index] + B[index];
      mark[index] = queueID;
    } else {
      return;
    } 
	}

	return;
}
