#ifndef STRUCTURE_HPP
#define STRUCTURE_HPP

#include "SharedMacros.h"

#include <atomic>
#include <thread>

/*
 * Data could be separate structure
 * Use "int" type for simplicity
 */
typedef int Task;

/*
 *  A node in a linked list is a pair of value
 *  and pointer to the next node
 */
typedef struct NodeStruct {
  struct NodeStruct* next;
  Task value;
} Node;

/*
 * a queue is basically a linked list
 * that has both head and tail pointers
 * and corresponding lock variables
 */
typedef struct {
  Node* head; // pointer to head
  Node* tail; // pointer to tail
  std::atomic_uint* enqLock;
  std::atomic_uint* deqLock;
} BlockingQueue;

/*
 * a free memory pool has a root pointing to the first slot
 * and nextFreeSlot is the index of the next available slot
 */
typedef struct {
  Node* root;                     // root pointer to the pool
  std::atomic_uint* nextFreeSlot; // index of the next free slot
} FreePool;

#endif
