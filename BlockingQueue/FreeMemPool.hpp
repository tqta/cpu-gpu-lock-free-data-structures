/*
 * This is a very basic memory management that will be adopted in GPU kernel
 * It now supports only allocate function
 * TODO David's fully functioning management will replace this
 */

#ifndef FREE_MEM_POOL_HPP
#define FREE_MEM_POOL_HPP

#include "oclDeviceSetup.hpp"
#include "Structures.hpp"

/*
 * construct an array of size MAX_FREE_SPACE nodes
 * initialize all free nodes
 */
FreePool* constructMemPool(const cl_context &context);

/*
 * return pointer to the next free node
 */
Node* allocateNode(const cl_context &context, FreePool* pool);

/*
 * deallocate a node: putting back a node to free pool
 */
void deallocateNode(const cl_context &context, Node* node, FreePool* pool);

/*
 * deallocate FreePool
 */
void deallocateFreePool(const cl_context &context, FreePool* pool);

#endif
