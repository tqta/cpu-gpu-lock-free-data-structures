#ifndef OCL_SETUP

#define OCL_SETUP
#include <CL/opencl.h>
#include <assert.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

/*
 * utility function returning error message in string
 */
std::string getOCLErrCode(cl_int err);

void checkOCLError(cl_int err);

/*
 * create platform (e.g., AMD)
 */
void createPlatform(cl_platform_id &platform, int platformID);

/*
 * create context + device
 */
void createContext(cl_context &context, const cl_platform_id &platform, cl_device_id &device, int deviceID);

/*
 * create command queue object
 */
void createCommandQueue(const cl_context &context, const cl_device_id &device, cl_command_queue &commandQueue);

/*
 * read .cl file
 */
char* readKernelSource(const char *fileName);

/*
 * take kernel object and compile it
 */
void compileKernel(const char* kernelFileName, const char* kernelName,
										const cl_context &context, cl_program &program, const cl_device_id &device,
										cl_kernel &kernel);

/*
 * create regular buffer
 */
cl_mem createBuffer(cl_context context, cl_mem_flags flags, size_t buffSize);

/*
 * copy data to device memory
 */
void writeToDevice(cl_command_queue commandQueue, cl_mem buffer, void* host_ptr, size_t buffSize);

/*
 * copy data from device memory
 */
void readFromDevice(cl_command_queue commandQueue, cl_mem buffer, void* host_ptr, size_t buffSize);

/*
 * map a memory buffer - CPU is allowed to access it after mapping succeeds
 */
void* mapBuffer(cl_command_queue commandQueue, cl_mem buffer, cl_map_flags flags, size_t buffSize);

/*
 * unmap a memory buffer - GPU is allowed to access it after unmapping succeeds
 */
void unmapBuffer(cl_command_queue commandQueue, cl_mem buffer, void* ptr);

/*
 * create SVM memory space
 */
template <class T>
inline T* allocateSVM(const cl_context &context, int n, size_t alignment = 0){
  T* ret = (T*) clSVMAlloc(context, CL_MEM_READ_WRITE|CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS, sizeof(T)*n, alignment);
  assert(ret);
  return ret;
}

/*
 * free ocl objects
 */
void freeCLObjects(cl_command_queue &commandQueue, cl_context &context, cl_program &program, cl_kernel &kernel);

#endif
