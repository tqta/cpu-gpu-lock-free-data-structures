#include "BlockingQueue.hpp"
#include "FreeMemPool.hpp"

/*
 * if try & acquire the lock successfully --> return true
 * otherwise, return false
 * This function is non-blocking
 */
inline bool try_lock(std::atomic_uint *lock){
  unsigned int expected = UNLOCKED;
  return lock->compare_exchange_weak(expected, LOCKED, std::memory_order_seq_cst,
                                      std::memory_order_relaxed);
}

/*
 * clear the lock
 */
inline void release_lock(std::atomic_uint *lock){
	lock->store(UNLOCKED, std::memory_order_release);
}

BlockingQueue* constructQueue(const cl_context &context, FreePool* memPool) {
  BlockingQueue* queue = (BlockingQueue*) allocateSVM<BlockingQueue*>(context, 1);

	assert(queue);

	// create an initial dummy node
	Node* initNode = (Node*) allocateNode(context, memPool);
	assert(initNode);

	initNode->value = INVALID_DATA;

	// initially both head and tail pointing to this node
  queue->head = initNode;
  queue->tail = initNode;

  // two locks guarding enqueue and dequeue
  queue->enqLock = allocateSVM<std::atomic_uint>(context, 1);
  queue->deqLock = allocateSVM<std::atomic_uint>(context, 1);

  // make sure all locks are cleared
  queue->enqLock->store(UNLOCKED);
  queue->deqLock->store(UNLOCKED);

  return queue;
}

bool enqueue_strong(const cl_context &context, BlockingQueue* queue, Node* newNode, int tid){
  bool done = false;
  do {
    if (newNode && try_lock(queue->enqLock)){
      // now we got the enqLock
      // std::cout << "thread " << tid << " got enqLock: " << newNode->value << std::endl;

      queue->tail->next = newNode;      // link node at the end of the linked list
      queue->tail = newNode;            // swing tail to node

      release_lock(queue->enqLock);     // unlock enqLock
      done = true;
    }

    // TODO this fence may not be necessary
 //   std::atomic_thread_fence(std::memory_order_seq_cst);
  } while (!done);

  return true;
}

bool enqueue_weak(const cl_context &context, BlockingQueue* queue, Node* newNode, int tid){
  bool flag = false;
	if (newNode && try_lock(queue->enqLock)){
		// now we got the enqLock
//		std::cout << "thread " << tid << " got enqLock: " << newNode->value << std::endl;

		queue->tail->next = newNode;			// link node at the end of the linked list
		queue->tail = newNode; 						// swing tail to node

		release_lock(queue->enqLock); 		// unlock enqLock
		flag =  true;
	}

	return flag;
}

bool dequeue_strong(const cl_context &context, FreePool* memPool, BlockingQueue* queue, Task* returnData, int tid){
  Node* node = nullptr;
	Node* new_head = nullptr;
	*returnData = INVALID_DATA;

  bool done = false;
  do {
    if (try_lock(queue->deqLock)){
      // now we got the deqLock

      node = queue->head;             // read head
      new_head = node->next;          // read next pointer

      if (new_head != nullptr){
        // the queue is not empty, so save the new_head->value
        *returnData = new_head->value;
        queue->head = new_head;       // swing head to the next node
      }

      release_lock(queue->deqLock);   // unlock deqLock
      done = true;                    // done!
//      std::cout << "thread " << tid << " got deqLock: " << *returnData << std::endl;
    }

    // TODO this fence may not be necessary
//    std::atomic_thread_fence(std::memory_order_seq_cst);
  } while (!done);

  if (node && new_head) deallocateNode(context, node, memPool);   // deallocate node
  return true;
}

bool dequeue_weak(const cl_context &context, FreePool* memPool, BlockingQueue* queue, Task* returnData, int tid){
  Node* node = nullptr;
  Node* new_head = nullptr;
  *returnData = INVALID_DATA;

  bool flag = false;

  if (try_lock(queue->deqLock)){
    // now we got the deqLock

    node = queue->head;           // read head
    new_head = node->next;        // read next pointer

    if (new_head != NULL) {
      // the queue is not empty
      *returnData = new_head->value;  // save the new_head->value
      queue->head = new_head;         // swing head to the next node
    }

    release_lock(queue->deqLock);     // unlock deqLock
    flag = true;
//    std::cout << "thread " << tid << " got deqLock: " << *returnData << std::endl;
  }

  if (node) deallocateNode(context, node, memPool);   // deallocate node
  return flag;
}

/*
 * free up memory space for the queue
 * assume: the queue has been drained
 */
void deallocateQueue(const cl_context &context, FreePool* memPool, BlockingQueue* queue){
  assert(memPool);
	assert(queue);

	// even the queue is empty, there is at least one dummy node in the queue
	assert(queue->head);

	deallocateNode(context, queue->head, memPool);

	queue->head = nullptr;
	queue->tail = nullptr;

	clSVMFree(context, queue->enqLock);
	clSVMFree(context, queue->deqLock);
	clSVMFree(context, queue);
}
