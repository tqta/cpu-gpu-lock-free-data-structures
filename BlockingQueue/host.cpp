/*
 * Implement task queuing framework for heterogeneous CPU/GPU platform
 * Each thread (either CPU or GPU thread) is able to pull task from
 * the queue, execute task and probably produce task into the queue
 */

#include <atomic>
#include <chrono>
#include <thread>
#include <vector>
#include <unordered_map>

#include "oclDeviceSetup.hpp"
#include "Structures.hpp"
#include "FreeMemPool.hpp"

#include "cTimer.h"
#include <cmath>
#include "BlockingQueue.hpp"

#include <iostream>
#include <fstream>

void test(int tid, std::unordered_map<Task, int> &myMap, cl_context &context, FreePool* memPool, BlockingQueue* queue);
void prepareGPUTest(const cl_context &context, int* actions, int* newValues, int* poppedNodes);
void processGPUOutput(int* actions, int* newValues, int* poppedNodes, std::unordered_map<Task, int>* maps);

#define REPEAT 3      // number of runs

int main(){
	// =============================== opencl vars ===============================
	cl_platform_id platform;
	cl_context context;
	cl_device_id device;
	cl_command_queue commandQueue;
	cl_program program;
	cl_kernel kernel;

  // ===================== setup opencl components =====================
  int deviceId = 0;     // chosen device
  int platformId = 0;   // chosen platform

  createPlatform(platform, platformId);
  createContext(context, platform, device, deviceId);
  createCommandQueue(context, device, commandQueue);
  compileKernel("Kernel.cl", "concurrentQueue", context, program, device, kernel);

#if nWorkGroups
  cl_event event;
  size_t wgsize = WG_SIZE;
  size_t globalSize = NDRANGE_SIZE;
#endif

  std::ofstream outputFile ("output.txt");
  assert(outputFile.is_open());

  for (unsigned int iter = 0; iter < REPEAT; ++iter){
    std::cout << "Run #" << iter << std::endl;
    // ===================== allocate structures =====================
    // create a free memory pool
    FreePool* memPool = constructMemPool(context);

    // create & initialize a queue
    BlockingQueue* queue = constructQueue(context, memPool);

#if nWorkGroups
    // extra structures for GPU kernel
    int* actions = (int*) allocateSVM<int>(context, nGPUTasks);
    assert(actions);

    int* newValues = (int*) allocateSVM<int>(context, nGPUTasks);
    assert(newValues);

    int* poppedNodes = (int*) allocateSVM<int>(context, nGPUTasks);
    assert(poppedNodes);
#endif

    std::atomic_int* gateOpened = (std::atomic_int*) allocateSVM<std::atomic_int>(context, 1);
    assert(gateOpened);
    gateOpened[0] = 0;

    int* poolOverflow = (int*) allocateSVM<int>(context, 1);
    assert(poolOverflow);
    poolOverflow[0] = 0;

#if nWorkGroups
    prepareGPUTest(context, actions, newValues, poppedNodes);

    // ===================== set up GPU arguments  =====================
    cl_int err = 0;
    err = clSetKernelArgSVMPointer(kernel, 0, (void*)memPool);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 1, (void*)queue);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 2, (void*)actions);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 3, (void*)newValues);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 4, (void*)poppedNodes);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 5, (void*)poolOverflow);
    checkOCLError(err);
    err = clSetKernelArgSVMPointer(kernel, 6, (void*)gateOpened);
    checkOCLError(err);
#endif

    // ===================== test code =====================
    /*
     * given a number of threads, each thread randomly either enqueues
     * a number to the queue or dequeues an element from the queue
     */

    /*
     * all enqueue & dequeue are tracked to verify the correctness
     */

    // each cpu thread and gpu has a corresponding map(Data --> count)
    std::unordered_map<Task, int> maps[nWorkUnits];

    // timers
    tHighResTimer totalTimer;

#if nWorkGroups
    // start GPU kernel
    std::cout << "Launching GPU kernel ... " << std::endl;
    err = clEnqueueNDRangeKernel(commandQueue, kernel, 1, 0, &globalSize, &wgsize, 0, NULL, &event);
    checkOCLError(err);

    // flush kernel command
    err = clFlush(commandQueue);
    checkOCLError(err);
#endif

    // start CPU threads
    std::vector<std::thread* > threads;

    tHighResTimer cpuTimer;

#if nWorkGroups
    while (!gateOpened->load(std::memory_order_acquire))          // wait until GPU thread
      ;
#endif

    for (size_t tid = 0u; tid < nCPUThreads; ++tid){
      threads.push_back(new std::thread(test, tid, std::ref(maps[nWorkGroups+tid]), std::ref(context),
                                          memPool, queue));
    }

    // synchronize all threads
    for (auto &thread: threads){
      thread->join();
    }

//    std::cout << "CPU threads completed in " << cpuTimer.GetDuration()*1e3 << " ms" << std::endl;
    outputFile << cpuTimer.GetDuration()*1e3 << ", ";

#if nWorkGroups
    // wait for GPU kernel
    err = clFinish(commandQueue);
    checkOCLError(err);
#endif

//    std::cout << "Total completion time " << totalTimer.GetDuration()*1e3 << " ms" << std::endl;
    outputFile << totalTimer.GetDuration()*1e3 << ", ";

#if nWorkGroups
    cl_ulong s, e;
    if (clWaitForEvents(1 , &event) == CL_SUCCESS){ // wait for kernel execution
      err = clGetEventProfilingInfo(event,CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &s, NULL);
      err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &e, NULL);
    }

//    std::cout << "GPU threads completed in " << static_cast<long long>(e - s)/1e6 << " ms" << std::endl;
    outputFile << static_cast<long long>(e - s)/1e6 << "\n";
#endif

    // check if overflow happened?
    if (poolOverflow[0]){
      std::cout << std::flush;
      std::cerr << "GPU run out of memory" << std::endl;
    } else {
#if nWorkGroups
      // process GPU outputs
      processGPUOutput(actions, newValues, poppedNodes, maps);
#endif

      // drain all remaining elements from the queue
      std::cout << "Draining queue ... " << std::endl;
      while (true) {
        Task returnData;

 #if TEST_STRONG_ENQ_DEQ
        // dequeue a Data from queue
        dequeue_strong(context, memPool, queue, &returnData, 0);
 #else
        while (!dequeue_weak(context, memPool, queue, &returnData, 0))
          ;
 #endif

        if (returnData == INVALID_DATA) break;

    //  	std::cout << "Drained " << returnData << std::endl;
        maps[0][returnData]--;
      }

      // merge all unordered maps together to maps[0]
      std::unordered_map<Task, int> masterMap;

      for (unsigned int i = 0; i < nWorkUnits; ++i){
        // go through each entry in maps[i]
        for (auto &entry : maps[i])
          masterMap[entry.first] += entry.second;
      }

      // check if values of all entries in maps[0] are 0
      bool flag = true;
      for (auto &entry : masterMap){
        if (entry.second != 0){
          std::cout << "Data " << entry.first << " still has "
                              << entry.second << " copies in the queue" << std::endl;
          flag = false;
        }
      }

      if (!flag)
        std::cout << "Failed!" << std::endl;
      else
        std::cout << "Passed!" << std::endl;
    }

    // ===================== free opencl objects =====================
    // deallocate queue
    deallocateQueue(context, memPool, queue);

    // deallocate memPool
    deallocateFreePool(context, memPool);

#if nWorkGroups
    clSVMFree(context, actions);
    clSVMFree(context, poppedNodes);
    clSVMFree(context, newValues);
#endif

    clSVMFree(context, gateOpened);
    clSVMFree(context, poolOverflow);

  }

  freeCLObjects(commandQueue, context, program, kernel);

  outputFile.close();

  return 0;
}

void test(int tid, std::unordered_map<Task, int> &myMap, cl_context &context, FreePool* memPool,
              BlockingQueue* queue){
  for (unsigned int repeat = 0; repeat < nTasksPerUnit; ++repeat){
		// generate random actions: enqueue & dequeue
		unsigned int action = rand() % 2;

		if (action == 0){	// enqueue
			// generate a random number to be enqueued
			Task newData = rand() % MAX;

      // create & initialize a new node
      Node* newNode = (Node*) allocateNode(context, memPool);
      assert(newNode);

      newNode->value = newData;
      newNode->next = nullptr;

#if TEST_STRONG_ENQ_DEQ
			// enqueue newData to queue
			enqueue_strong(context, queue, newNode, tid);
#else
			while (!enqueue_weak(context, queue, newNode, tid))
			  ;
#endif

			// record the number in myMap
			myMap[newData]++;
		} else {					// dequeue
		  Task returnData;

#if TEST_STRONG_ENQ_DEQ
			// dequeue a Data from queue
			dequeue_strong(context, memPool, queue, &returnData, tid);
#else
			while (!dequeue_weak(context, memPool, queue, &returnData, tid))
			  ;
#endif

			if (returnData != INVALID_DATA){
				myMap[returnData]--;
			}
		}
	}
}

void prepareGPUTest(const cl_context &context, int* actions, int* newValues, int* poppedNodes){
  for (unsigned int i = 0; i < nGPUTasks; i++){
    newValues[i] = rand() % MAX;
    actions[i] = rand() % 2;
    poppedNodes[i] = INIT_NUMBER;
  }
}

void processGPUOutput(int* actions, int* newValues, int* poppedNodes, std::unordered_map<Task, int>* maps){
  std::cout << "Processing GPU output ... " << std::endl;

  for (unsigned int i = 0; i < nGPUTasks; i++){
    if (actions[i] == 0) {
//      std::cout << "GPU thread " << i << " enqueued - action = " << actions[i]
//                << " - value = " << newValues[i] << std::endl;
      maps[i/nTasksPerUnit][newValues[i]]++;
    } else {
//      std::cout << "GPU thread " << i << " dequeued - action = " << actions[i]
//                << " - popped value = " << poppedNodes[i] << std::endl;
      if (poppedNodes[i] != INVALID_DATA)
        maps[i/nTasksPerUnit][poppedNodes[i]]--;
    }
  }
}
