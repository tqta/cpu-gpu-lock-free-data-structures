#ifndef NODE_HPP
#define NODE_HPP

#include "oclDeviceSetup.hpp"
#include "Structures.hpp"

/*
 * construct and initialize an empty list
 */
BlockingQueue* constructQueue(const cl_context &context, FreePool* memPool);

/*
 * enqueue_strong: insert newData to a given queue
 * 					guarantee the insertion will eventually succeed
 * assume: newNode is allocated by the caller
 */
bool enqueue_strong(const cl_context &context, BlockingQueue* queue, Node* newNode, int tid);

/*
 * enqueue_weak: insert newData to a given queue
 * 					the insertion may fail if thread calling the function fails to
 * 					compete with other threads
 * assume: newNode is allocated by the caller
 */
bool enqueue_weak(const cl_context &context, BlockingQueue* queue, Node* newNode, int tid);

/*
 * dequeue_strong: pop the head element from a given queue
 * 					guarantee the pop will eventually succeed
 * 					(either return an element if the queue is not empty or
 * 					return INVALID_DATA otherwise)
 */
bool dequeue_strong(const cl_context &context, FreePool* memPool, BlockingQueue* queue, Task* returnData, int tid);

/*
 * dequeue_weak: pop the head element from a given queue
 * 					the pop may fail if thread calling the function fails to
 * 					compete with other threads
 * 				if the calling thread fails to compete --> return false and returnData = INVALID_DATA
 *				otherwise
 *					if the queue is empty --> return true and returnData is INVALID_DATA
 *					if the queue is not empty --> return true and returnData = Head's value
 */
bool dequeue_weak(const cl_context &context, FreePool* memPool, BlockingQueue* queue, Task* returnData, int tid);

/*
 * free up memory space for the queue
 * assume: the queue has been drained
 */
void deallocateQueue(const cl_context &context, FreePool* memPool, BlockingQueue* queue);

#endif
