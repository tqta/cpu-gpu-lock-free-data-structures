#include "FreeMemPool.hpp"

FreePool* constructMemPool(const cl_context &context){
  // allocate a FreePool structure
  FreePool* pool = (FreePool*) allocateSVM<FreePool>(context, 1);
  assert(pool);

  // allocate MAX_FREE_SPACE nodes
  pool->root = (Node*) allocateSVM<Node>(context, MAX_FREE_SPACE);
  assert(pool->root);

  pool->nextFreeSlot = (std::atomic_uint*) allocateSVM<std::atomic_uint>(context, 1);
  assert(pool->nextFreeSlot);

  pool->nextFreeSlot->store(0, std::memory_order_relaxed);

  return pool;
}

Node* allocateNode(const cl_context &context, FreePool* pool){
  // do an atomic increment on nextFreeSlot
  unsigned int mySlot = pool->nextFreeSlot->fetch_add(1, std::memory_order_acq_rel);

  if (mySlot < MAX_FREE_SPACE){
    // my slot has been reserved successfully
    return (pool->root + mySlot);
  } else {
    return nullptr;
  }
}

void deallocateNode(const cl_context &context, Node* node, FreePool* pool){
  /*
   *  TODO this function is not complete now
   *  putting back node to free pool requires a more complex structure like concurrent queue
   */

  node = nullptr;
}

void deallocateFreePool(const cl_context &context, FreePool* pool){
  clSVMFree(context, pool->nextFreeSlot);
  pool->nextFreeSlot = nullptr;

  clSVMFree(context, pool->root);
  pool->root = nullptr;

  clSVMFree(context, pool);
  pool = nullptr;
}
