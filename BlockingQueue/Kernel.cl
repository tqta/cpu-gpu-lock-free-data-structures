#include "SharedMacros.h"

#define true 1
#define false 0

/*************************************
 * OpenCL version of Structures.hpp
 *************************************/

typedef int Data;

/*
 *  A node in a linked list is a pair of value
 *  and pointer to the next node
 */
typedef struct NodeStruct {
  __global struct NodeStruct* next;
  Data value;
} Node;

/*
 * a queue is basically a linked list
 * that has both head and tail pointers
 * and corresponding lock variables
 */
typedef struct {
  __global Node* head; // pointer to head
  __global Node* tail; // pointer to tail
  __global volatile atomic_uint* enqLock;
  __global volatile atomic_uint* deqLock;
} BlockingQueue;

/*
 * a free memory pool has a root pointing to the first slot
 * and nextFreeSlot is the index of the next available slot
 */
typedef struct {
  __global Node* root;                         // root pointer to the pool
  __global volatile atomic_uint* nextFreeSlot; // index of the next free slot
} FreePool;

/*************************************
 * OpenCL version of FreeMemPool.hpp
 *************************************/

/*
 * return pointer to the next free node
 */
__global Node* allocateNode(__global volatile FreePool* pool){
  // do an atomic increment on nextFreeSlot
  unsigned int mySlot = atomic_fetch_add_explicit(pool->nextFreeSlot, 1, memory_order_acq_rel, memory_scope_all_svm_devices);

  if (mySlot < MAX_FREE_SPACE){
    // my slot has been reserved successfully
    return (pool->root + mySlot);
  } else {
    return NULL;
  }
}

/*
 * deallocate a node: putting back a node to free pool
 */
void deallocateNode(__global Node* node, __global volatile FreePool* pool){
  /*
   *  TODO this function is not complete now
   *  putting back node to free pool requires a more complex structure like concurrent queue
   */

  node = NULL;
}

/*************************************
 * OpenCL version of ConcurrentQueue.hpp
 *************************************/

/*
 * if try & acquire the lock successfully --> return true
 * otherwise, return false
 * This function is non-blocking
 */
bool try_lock(__global volatile atomic_uint *lock){
  unsigned int expected = UNLOCKED;
	return atomic_compare_exchange_weak_explicit(lock, &expected, LOCKED, 
                                                memory_order_seq_cst, 
                                                memory_order_relaxed, 
                                                memory_scope_all_svm_devices);
}

/*
 * clear the lock
 */
void release_lock(__global volatile atomic_uint *lock){
  atomic_store_explicit(lock, UNLOCKED, memory_order_release, memory_scope_all_svm_devices);
}

/*
 * enqueue_strong: insert newData to a given queue
 * 					guarantee the insertion will eventually succeed
 * assume: newNode is allocated by the caller
 */
bool enqueue_strong(__global volatile BlockingQueue* queue, __global Node* newNode){
  bool done = false;
  do {
		if (newNode && try_lock(queue->enqLock)){ 			
      // now we got the enqLock

			queue->tail->next = newNode;			// link node at the end of the linked list
			queue->tail = newNode; 						// swing tail to node

			release_lock(queue->enqLock); 		// unlock enqLock

      done = true;
		}
		// failed threads keep trying to acquire enqLock until success

  	atomic_work_item_fence(CLK_GLOBAL_MEM_FENCE, memory_order_seq_cst, memory_scope_all_svm_devices);
	} while (!done);

  return true;
}

/*
 * enqueue_weak: insert newData to a given queue
 * 					the insertion may fail if thread calling the function fails to
 * 					compete with other threads
 * assume: newNode is allocated by the caller
 */
bool enqueue_weak(__global volatile BlockingQueue* queue, __global Node* newNode){
  bool flag = false;

	if (newNode && try_lock(queue->enqLock)){
		// now we got the enqLock

		queue->tail->next = newNode;			// link node at the end of the linked list
		queue->tail = newNode; 						// swing tail to node

		release_lock(queue->enqLock); 		// unlock enqLock

		flag = true;
	}

  return flag;
}

/*
 * dequeue_strong: pop the head element from a given queue
 * 					guarantee the pop will eventually succeed
 * 					(either return an element if the queue is not empty or
 * 					return INVALID_DATA otherwise)
 */
bool dequeue_strong(__global volatile FreePool* memPool, __global volatile BlockingQueue* queue, __global Data* returnData){
  __global Node* node = NULL;
	__global Node* new_head = NULL;
	*returnData = INVALID_DATA;

  bool done = false;
  do { 
		if (try_lock(queue->deqLock)){
			// now we got the deqLock

			node = queue->head; 					  // read head
			new_head = node->next; 				  // read next pointer

			if (new_head != NULL){
			  // the queue is not empty, so save the new_head->value
			  *returnData = new_head->value;
			  queue->head = new_head; 			// swing head to the next node
      }

			release_lock(queue->deqLock);   // unlock deqLock
			done = true;								    // done!
		}

		atomic_work_item_fence(CLK_GLOBAL_MEM_FENCE, memory_order_seq_cst, memory_scope_all_svm_devices);
  } while (!done);

  if (node && new_head) deallocateNode(node, memPool);    // deallocate node
  return true;
}

/*
 * dequeue_weak: pop the head element from a given queue
 * 					the pop may fail if thread calling the function fails to
 * 					compete with other threads
 * 				if the calling thread fails to compete --> return false and returnData = INVALID_DATA
 *				otherwise
 *					if the queue is empty --> return true and returnData is INVALID_DATA
 *					if the queue is not empty --> return true and returnData = Head's value
 */
bool dequeue_weak(__global volatile FreePool* memPool, __global volatile BlockingQueue* queue, __global Data* returnData){
  __global Node* node = NULL;
  __global Node* new_head = NULL;
  *returnData = INVALID_DATA;

  bool flag = false;

  if (try_lock(queue->deqLock)){
    // now we got the deqLock

    node = queue->head;           // read head
    new_head = node->next;        // read next pointer
   
    if (new_head != NULL) {
      // the queue is not empty
      *returnData = new_head->value;  // save the new_head->value
      queue->head = new_head;         // swing head to the next node
    }

    release_lock(queue->deqLock);     // unlock deqLock
    flag = true;
  }

  if (node != NULL) deallocateNode(node, memPool);    // deallocate node

  return flag;
}

/* ========================= Main kernel ================================== */
/*
 *  each thread calls either enqueue or dequeue function
 */
__kernel void concurrentQueue(__global volatile FreePool* memPool,
                              __global volatile BlockingQueue* queue,   
                              __global const int* actions,    
                              __global const int* newValues,      // values to be pushed to the queue
                              __global int* poppedNodes,          // list of numbers popped from the queue
                              __global int* poolOverflow,         // simple flag showing if we're running out of free space
                              __global atomic_int* gateOpened     // flag showing whether GPU threads already started
){
  int globalThreadID = get_global_id(0);
  int localThreadID = get_local_id(0);
  int workUnitID = get_group_id(0);

  if (globalThreadID == 0) 
    atomic_store_explicit(gateOpened, true, memory_order_release, memory_scope_all_svm_devices); // signal CPU

  for (int i = 0; i < nTasksPerGPUThread; i++){
    int myIndex = (workUnitID*nTasksPerUnit) + WG_SIZE*i + localThreadID;

    int myAction = actions[myIndex];

    if (myAction == ENQUEUE){  // call enqueue
      __global Node* newNode = allocateNode(memPool);   // make a new node
      if (!newNode) {
        // we can't do anything other than giving up GPU resource
        poolOverflow[0] = true;
        return;     
      }

      newNode->value = newValues[myIndex];
      newNode->next = NULL;

      // enqueue the new node
  #if TEST_STRONG_ENQ_DEQ
      enqueue_strong(queue, newNode);                   
  #else
      bool flag = false;
      do {
        flag = enqueue_weak(queue, newNode);
      	atomic_work_item_fence(CLK_GLOBAL_MEM_FENCE, memory_order_seq_cst, memory_scope_all_svm_devices);
      } while (!flag);
  #endif
    } else {                  // call dequeue
      // dequeue head node
  #if TEST_STRONG_ENQ_DEQ
      dequeue_strong(memPool, queue, poppedNodes+myIndex);  
  #else
      bool flag = false;
      do {
        flag = dequeue_weak(memPool, queue, poppedNodes+myIndex);
      	atomic_work_item_fence(CLK_GLOBAL_MEM_FENCE, memory_order_seq_cst, memory_scope_all_svm_devices);
      } while (!flag);
  #endif
    }
  }

  return;
}
