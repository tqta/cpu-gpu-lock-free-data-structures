#include "oclDeviceSetup.hpp"

/*
 * utility function returning error message in string
 */
std::string getOCLErrCode(cl_int err){
  switch (err) {
    case CL_SUCCESS:                            return "Success!";
    case CL_DEVICE_NOT_FOUND:                   return "Device not found.";
    case CL_DEVICE_NOT_AVAILABLE:               return "Device not available";
    case CL_COMPILER_NOT_AVAILABLE:             return "Compiler not available";
    case CL_MEM_OBJECT_ALLOCATION_FAILURE:      return "Memory object allocation failure";
    case CL_OUT_OF_RESOURCES:                   return "Out of resources";
    case CL_OUT_OF_HOST_MEMORY:                 return "Out of host memory";
    case CL_PROFILING_INFO_NOT_AVAILABLE:       return "Profiling information not available";
    case CL_MEM_COPY_OVERLAP:                   return "Memory copy overlap";
    case CL_IMAGE_FORMAT_MISMATCH:              return "Image format mismatch";
    case CL_IMAGE_FORMAT_NOT_SUPPORTED:         return "Image format not supported";
    case CL_BUILD_PROGRAM_FAILURE:              return "Program build failure";
    case CL_MAP_FAILURE:                        return "Map failure";
    case CL_INVALID_VALUE:                      return "Invalid value";
    case CL_INVALID_DEVICE_TYPE:                return "Invalid device type";
    case CL_INVALID_PLATFORM:                   return "Invalid platform";
    case CL_INVALID_DEVICE:                     return "Invalid device";
    case CL_INVALID_CONTEXT:                    return "Invalid context";
    case CL_INVALID_QUEUE_PROPERTIES:           return "Invalid queue properties";
    case CL_INVALID_COMMAND_QUEUE:              return "Invalid command queue";
    case CL_INVALID_HOST_PTR:                   return "Invalid host pointer";
    case CL_INVALID_MEM_OBJECT:                 return "Invalid memory object";
    case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:    return "Invalid image format descriptor";
    case CL_INVALID_IMAGE_SIZE:                 return "Invalid image size";
    case CL_INVALID_SAMPLER:                    return "Invalid sampler";
    case CL_INVALID_BINARY:                     return "Invalid binary";
    case CL_INVALID_BUILD_OPTIONS:              return "Invalid build options";
    case CL_INVALID_PROGRAM:                    return "Invalid program";
    case CL_INVALID_PROGRAM_EXECUTABLE:         return "Invalid program executable";
    case CL_INVALID_KERNEL_NAME:                return "Invalid kernel name";
    case CL_INVALID_KERNEL_DEFINITION:          return "Invalid kernel definition";
    case CL_INVALID_KERNEL:                     return "Invalid kernel";
    case CL_INVALID_ARG_INDEX:                  return "Invalid argument index";
    case CL_INVALID_ARG_VALUE:                  return "Invalid argument value";
    case CL_INVALID_ARG_SIZE:                   return "Invalid argument size";
    case CL_INVALID_KERNEL_ARGS:                return "Invalid kernel arguments";
    case CL_INVALID_WORK_DIMENSION:             return "Invalid work dimension";
    case CL_INVALID_WORK_GROUP_SIZE:            return "Invalid work group size";
    case CL_INVALID_WORK_ITEM_SIZE:             return "Invalid work item size";
    case CL_INVALID_GLOBAL_OFFSET:              return "Invalid global offset";
    case CL_INVALID_EVENT_WAIT_LIST:            return "Invalid event wait list";
    case CL_INVALID_EVENT:                      return "Invalid event";
    case CL_INVALID_OPERATION:                  return "Invalid operation";
    case CL_INVALID_GL_OBJECT:                  return "Invalid OpenGL object";
    case CL_INVALID_BUFFER_SIZE:                return "Invalid buffer size";
    case CL_INVALID_MIP_LEVEL:                  return "Invalid mip-map level";
    default: return "Unknown";
  }
}

void checkOCLError(cl_int err) {
  if (err != CL_SUCCESS){
    std::cout << getOCLErrCode(err) << std::endl;
    exit(-1);
  }
}

/*
 * create platform (e.g., AMD)
 */
void createPlatform(cl_platform_id &platform, int platformID){
  unsigned int platformCount = 0;
  //clGetPlatformIDs(cl_uint num_entries, cl_platform_id *platforms, cl_uint *num_platforms)
  cl_int err = clGetPlatformIDs(2, NULL, &platformCount);

  cl_platform_id platforms[platformCount];

  err = clGetPlatformIDs(2, platforms, NULL);

  checkOCLError(err);

  // ---------- Print device info ----------
  printf("***** Available platforms *****\n");
  for (unsigned int i = 0; i < platformCount; i++){
    printf("--- Platform # %d ---\n", i);
    char buffer[10240];
    err = clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, sizeof(buffer), buffer, NULL);
    printf("  PLATFORM_NAME = %s\n", buffer);
  }

  printf(">>> Choose platform %d\n", platformID);

  platform = platforms[platformID];
}

/*
 * create context + device
 */
void createContext(cl_context &context, const cl_platform_id &platform, cl_device_id &device, int deviceID){
  cl_int err = 0;
  unsigned int deviceCount = 0;

  // ---------- Get devices ----------
  err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, NULL, &deviceCount);

  printf("Number of devices: %d\n", deviceCount);

  // free devices
  cl_device_id devices[deviceCount];

  err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, deviceCount, devices, NULL);

  checkOCLError(err);

  // ---------- Print device info ----------
  printf("***** Available devices *****\n");
  for (unsigned int i = 0; i < deviceCount; i++){
    printf("--- Device # %d ---\n", i);
    char buffer[10240];
    err = clGetDeviceInfo(devices[i], CL_DEVICE_NAME, sizeof(buffer), buffer, NULL);
    printf("  DEVICE_NAME = %s\n", buffer);
/*    err = clGetDeviceInfo(devices[i], CL_DEVICE_VENDOR, sizeof(buffer), buffer, NULL);*/
/*    printf("  DEVICE_VENDOR = %s\n", buffer);*/
/*    err = clGetDeviceInfo(devices[i], CL_DEVICE_VERSION, sizeof(buffer), buffer, NULL);*/
/*    printf("  DEVICE_VERSION = %s\n", buffer);*/
/*    err = clGetDeviceInfo(devices[i], CL_DRIVER_VERSION, sizeof(buffer), buffer, NULL);*/
/*    printf("  DRIVER_VERSION = %s\n", buffer);*/
/*    err = clGetDeviceInfo(devices[i], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(buf_uint), &buf_uint, NULL);*/
/*    printf("  DEVICE_MAX_COMPUTE_UNITS = %u\n", (unsigned int)buf_uint);*/
/*    err = clGetDeviceInfo(devices[i], CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(buf_uint), &buf_uint, NULL);*/
/*    printf("  DEVICE_MAX_CLOCK_FREQUENCY = %u\n", (unsigned int)buf_uint);*/
/*    err = clGetDeviceInfo(devices[i], CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(buf_ulong), &buf_ulong, NULL);*/
/*    printf("  DEVICE_GLOBAL_MEM_SIZE = %llu\n", (unsigned long long)buf_ulong);*/
/*    err = clGetDeviceInfo(devices[i], CL_DEVICE_LOCAL_MEM_SIZE, sizeof(buf_ulong), &buf_ulong, NULL);*/
/*    printf("  DEVICE_LOCAL_MEM_SIZE = %llu\n", (unsigned long long)buf_ulong);*/
/*    err = clGetDeviceInfo(devices[i], CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, sizeof(buf_ulong), &buf_ulong, NULL);*/
/*    printf("  DEVICE_GLOBAL_MEM_CACHE_SIZE = %llu\n", (unsigned long long)buf_ulong);*/
  }

  if (deviceID >= (int)deviceCount){
    printf("Invalid device id\n");
    exit(-1);
  }

  printf(">>> Choose device %d\n", deviceID);

  device = devices[deviceID];

  context = clCreateContext(0, 1, &device, NULL, NULL, &err);

  checkOCLError(err);
}

/*
 * create command queue object
 */
void createCommandQueue(const cl_context &context, const cl_device_id &device, cl_command_queue &commandQueue){
  cl_int err = 0;

  // Create device command queue
  cl_queue_properties prop[] = {CL_QUEUE_PROPERTIES, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE|CL_QUEUE_PROFILING_ENABLE,0};

  commandQueue = clCreateCommandQueueWithProperties(context, device, prop, &err);

  checkOCLError(err);
}

/*
 * read .cl file
 */
char* readKernelSource(const char *fileName){
  /*
   * Load the kernel source code into *char source
   */
  FILE *fp;
  char *source;
  size_t size;

  fp = fopen(fileName, "r");
  if (!fp) {
    fprintf(stderr, "Failed to load kernel.\n");
    exit(1);
  }

  // Measure the size of source file
  fseek(fp, 0, SEEK_END);
  size = ftell(fp);

  fseek(fp, 0, SEEK_SET);

  source = (char*)malloc(size+1);
  size = fread(source, 1, size, fp);
  source[size]='\0';
  fclose( fp );

  return source;
}

/*
 * take kernel object and compile it
 */
void compileKernel(const char* kernelFileName, const char* kernelName,
										const cl_context &context, cl_program &program, const cl_device_id &device,
										cl_kernel &kernel){
  cl_int err = 0;

  // ---------- Read the OpenCL kernel from source file ----------
  char* source = readKernelSource(kernelFileName);

  // ---------- Create a program with kernel source file ----------
  program = clCreateProgramWithSource(context, 1, (const char **)&source, NULL, &err);

  checkOCLError(err);

  // ---------- Build the program ----------
  err = clBuildProgram(program, 1, &device, "-save-temps -cl-std=CL2.0 -I./", NULL, NULL);

  if (err != CL_SUCCESS){
  	std::cout << "Error in building kernel ... " << std::endl;

    size_t build_log_size;
    err = clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &build_log_size);

    char log[build_log_size];
    err = clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, build_log_size, log, NULL);

    printf("%s\n", log);
    exit(-1);
  }

  // ---------- Create kernel ----------
  kernel = clCreateKernel(program, kernelName, &err);

  checkOCLError(err);

  // --------- Free source -----------
  if (source != nullptr) free(source);
}

/*
 * create regular buffer
 */
cl_mem createBuffer(cl_context context, cl_mem_flags flags, size_t buffSize){
  cl_int err = 0;
  cl_mem buffer = clCreateBuffer(context, flags, buffSize, NULL, &err);

  checkOCLError(err);

  return buffer;
}

/*
 * copy data to device memory
 */
void writeToDevice(cl_command_queue commandQueue, cl_mem buffer, void* host_ptr, size_t buffSize){
  cl_int err = clEnqueueWriteBuffer(commandQueue, buffer, CL_FALSE, 0, buffSize, host_ptr, 0, NULL, NULL);

  checkOCLError(err);

  clFinish(commandQueue);
}

/*
 * copy data from device memory
 */
void readFromDevice(cl_command_queue commandQueue, cl_mem buffer, void* host_ptr, size_t buffSize){
  cl_int err = clEnqueueReadBuffer(commandQueue, buffer, CL_FALSE, 0, buffSize, host_ptr, 0, NULL, NULL);

  checkOCLError(err);

  clFinish(commandQueue);
}

/*
 * map a memory buffer - CPU is allowed to access it after mapping succeeds
 */
void* mapBuffer(cl_command_queue commandQueue, cl_mem buffer, cl_map_flags flags, size_t buffSize){
  cl_int err = 0;
  void* ptr = clEnqueueMapBuffer(commandQueue, buffer, CL_TRUE, flags,
                  0, buffSize, 0u, NULL, NULL, &err);

  checkOCLError(err);

  return ptr;
}

/*
 * unmap a memory buffer - GPU is allowed to access it after unmapping succeeds
 */
void unmapBuffer(cl_command_queue commandQueue, cl_mem buffer, void* ptr){
  cl_int err = clEnqueueUnmapMemObject(commandQueue, buffer, ptr, 0u, NULL, NULL);

  checkOCLError(err);
}

/*
 * free ocl objects
 */
void freeCLObjects(cl_command_queue &commandQueue, cl_context &context, cl_program &program, cl_kernel &kernel){
  // release OpenCL objects
  if(commandQueue)clReleaseCommandQueue(commandQueue);
  if(context)clReleaseContext(context);
  if(program)clReleaseProgram(program);
  if(kernel)clReleaseKernel(kernel);

  // free mem pointers
//  free(device);
}
