/*
	Implementation of concurrent linked list on GPU.
	This linked list represents a set --> Nodes contain distinct values
	The list is sorted in ascending order

	Listsize is defined as a macro
	memory pool size is ndrange size = WGSIZE * WGNUM.
*/

#include "linked_list_host.hpp"
//#include <math.h>
//#include <atomic>

// ===================== Main =======================
int main(){

	// ========== Setup OCL components ==============
	int deviceId = 0;		// chosen device
	int platformId = 0;		// chosen platform

	cl_event event;
	cl_ulong start, end;
	double elapse;


	createPlatform(&platform, platformId);
	createContext(&context, platform,&device, deviceId);
	createCommandQueue(context, device, &commandQueue);
	compileKernel("kernel.cl", "linkedlist", context, &program, &device, &kernel);

	size_t wgsize = WGSIZE;
	// size_t wgnum = WGNUM;
	size_t globalSize = GPUSHARE;

	// ========= host linked list ==========
	int listSize = LISTSIZE;
	Node* linkedListHead = createLinkedList(listSize);

	printf("========== Before ===========\n");
#ifdef PRINTBEFORE
	printList(linkedListHead);
#endif


	// ========= free memory pool ==========
	int poolSize = WORKLOAD;

//	Node* freeMemPool = createFreeMemPool(poolSize);
	Node* freeMemPool = createFreeMemPool(poolSize);

	std::atomic<int>* nextFreeSlot = (std::atomic<int>*) clSVMAlloc(context, CL_MEM_READ_WRITE|CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS, sizeof(int), 0);
	*nextFreeSlot = 0;

	// ========= populate search keys ======
	std::string keyfilename = "./randomeKeys_" + std::to_string(MAXKEY) + ".txt";

	int numKeys = WORKLOAD; // TODO DEBUGGING: 1 key per thread
	cl_mem devKeys = nullptr;
	int* hostKeys = createKeys(keyfilename,numKeys);
	cl_mem devActions = nullptr;
	int* hostActions = nullptr;
	cl_mem devOutput = nullptr;
	int* hostOutput = nullptr;

#if USE_GPU
	devKeys = createBuffer(context, CL_MEM_READ_ONLY, sizeof(int) * numKeys);

	//populart action(add, remove, search) input
	int numActions = WORKLOAD;
	devActions = createBuffer(context, CL_MEM_READ_ONLY, sizeof(int) * numActions);
	hostActions = createActions("./randomeActions.txt",numActions);

	// ========= output buffer =============
	// each thread writes its output to this buffer. One per thread
	devOutput = createBuffer(context, CL_MEM_WRITE_ONLY, sizeof(int) * globalSize);
	hostOutput = (int*) malloc (sizeof(int) * globalSize);

	for (int i = 0; i < globalSize ; i++)
		hostOutput[i] = 0;
#endif

	// ========= address buffer ============
	// TODO DEBUGGING: buffer holding memory address. One per thread
	// cl_mem devAddress = createBuffer(context, CL_MEM_WRITE_ONLY, sizeof(unsigned long) * globalSize);
	// unsigned long* hostAddress = (unsigned long*) malloc (sizeof(unsigned long) * globalSize);

	// for (int i = 0; i < globalSize ; i++)
	// 	hostAddress[i] = 0;


	int* CPUresult = (int*) malloc (sizeof(int) * CPUSHARE);

#if USE_GPU
	writeToDevice(commandQueue, devKeys, hostKeys, sizeof(int) * numKeys);
	writeToDevice(commandQueue, devActions, hostActions, sizeof(int) * numActions);
	writeToDevice(commandQueue, devOutput, hostOutput, sizeof(int) * globalSize);
	// writeToDevice(commandQueue, devAddress, hostAddress, sizeof(unsigned long) * globalSize);

	// ========= set kernel arguments ======
	err = clSetKernelArgSVMPointer(kernel, 0, (void*)linkedListHead);
	err = clSetKernelArgSVMPointer(kernel, 1, (void*)freeMemPool);
	err = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void*)&devKeys);
	err = clSetKernelArg(kernel, 3, sizeof(cl_mem), (void*)&devActions);
	err = clSetKernelArg(kernel, 4, sizeof(cl_mem), (void*)&devOutput);
	// err = clSetKernelArg(kernel, 5, sizeof(cl_mem), (void*)&devAddress);
	err = clSetKernelArgSVMPointer(kernel, 5, (void*)nextFreeSlot);
//	err = clSetKernelArgSVMPointer(kernel, 7, (unsigned long*)svmAddr);

	if (err != CL_SUCCESS) {
		printf("Error in clSetKernelArgSVMPointer: %s\n",
		checkError(err));
		exit(-1);
	}
#endif

	for(int i = 0; i < CPUSHARE; i++){
		CPUresult[i] = 0;
	}

	double cpu_gpu_time = 0;
	double c_g_begin = getTime();

	//spawn CPU threads & run CPU worker threads
#if USE_CPU
	printf(">>>Launching CPU worker threads...\n");
	std::vector<std::thread> cpu_threads;
	for(unsigned int i = 0; i < CPU_THREADS; i++){
		//spawning thraeds
		printf("thread # %d created. \n",i);
		cpu_threads.push_back(std::thread([=](){
			//put cpu functions here.
#define WORKLOAD_PER_THREAD CPUSHARE/CPU_THREADS

			for(int j = 0; j < WORKLOAD_PER_THREAD; j++){
				//put add result into CPUresult array. i is threadid. and since we are launching 4 threads. We only need to iterate CPUSHARE / 4 times.
				CPUresult[i*WORKLOAD_PER_THREAD + j] = add(linkedListHead, freeMemPool, hostKeys[i+globalSize], nextFreeSlot);
			}

		}));
	}
#endif

#if USE_GPU
	//After spawning CPU worker threads, worker threads will launch GPU
	printf(">>>Launching GPU...\n");
	err = clEnqueueNDRangeKernel(commandQueue, kernel, 1, 0, &globalSize, &wgsize, 0, NULL, &event);
	if (err != CL_SUCCESS) {
		printf("Error in clEnqueueNDRangeKernel: %s\n",
		checkError(err));
		exit(-1);
	}

	// must do clFlush to guarantee all queued commands are issued to appropriate devices.
	err = clFlush(commandQueue);
	if (err != CL_SUCCESS) {
		printf("Error in clFlush: %s\n",
		checkError(err));
		exit(-1);
	}
#endif

	//stop CPU worker threads
	std::for_each(cpu_threads.begin(), cpu_threads.end(), [](std::thread &t){
		t.join();
	});

#if USE_GPU
	//stop GPU threads
	err = clFinish(commandQueue);
	if (err != CL_SUCCESS) {
		printf("Error in clFinish: %s\n",
		checkError(err));
		exit(-1);
	}
#endif

	double c_g_end = getTime();
	cpu_gpu_time = (c_g_end - c_g_begin);

//get timing info
	// double cpu_gpu_time = timer.GetDuration()*1000.0;


#if USE_GPU
	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL);
	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL);

	elapse = (end - start) /  1000; // in usec.
#endif

	printf("\n =================CPU + GPU====================\n");
	printf("cpu and gpu share the workload, gpu does the first %d share, cpu does the second %d share.\n",GPUSHARE, CPUSHARE);
	printf("total run time: %f microseconds\n", cpu_gpu_time);
	// printf("cpu run time: %f microseconds\n", c_time);
  // printf("join time: %f microseconds.\n", join_time);
#if USE_GPU
	printf("kernel run time: %f microseconds.\n", elapse);
#endif

	printf("_________________________________________________\n");

#if USE_GPU
#ifdef GPUONLY
		size_t gpuGLobalSize = globalSize * 4;
		//reset freeMemPool so that it is ready for gpu work
		for(int i = 0; i < poolSize; i ++){
			freeMemPool[i].value = 0;
			atomic_store_explicit(&(freeMemPool[i].nextAndMark),combine(0,0),std::memory_order_seq_cst);
		}
		//reset linked list head
		atomic_store_explicit(&(linkedListHead->nextAndMark),combine(0,0),std::memory_order_seq_cst);
		//reset nextFreeSlot
		*nextFreeSlot = 0;

		printf("\n after reset, printing list...(should be empty list)\n");
		printList(linkedListHead);
		err = clSetKernelArgSVMPointer(kernel, 0, (void*)linkedListHead);
		err = clSetKernelArgSVMPointer(kernel, 1, (void*)freeMemPool);
		err = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void*)&devKeys);
		err = clSetKernelArg(kernel, 3, sizeof(cl_mem), (void*)&devActions);
		err = clSetKernelArg(kernel, 4, sizeof(cl_mem), (void*)&devOutput);
		err = clSetKernelArg(kernel, 5, sizeof(cl_mem), (void*)&devAddress);
		err = clSetKernelArgSVMPointer(kernel, 6, (void*)nextFreeSlot);

		err = clEnqueueNDRangeKernel(commandQueue, kernel, 1, 0, &gpuGLobalSize, &wgsize, 0, NULL, &event);
		if (err != CL_SUCCESS) {
			printf("Error in clEnqueueNDRangeKernel: %s\n",
			checkError(err));
			exit(-1);
		}
		clFinish(commandQueue);

		clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL);
		clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL);

		elapse = (end - start) /  1000; // in usec.
		printf("\n =================GPU ONLY=====================\n");
		printf("rerunning gpu to do all workload.\n");
		printf("kernel run time: %f microseconds.\n", elapse);
		printf("_________________________________________________\n");

		// printList(linkedListHead);
#endif
#endif

	// ========= Verify output =============

		printf("========== After (linked list) ===========\n");
#ifdef PRINTAFTER
		printList(linkedListHead);
#endif

	printf("======================================\n");
#if USE_GPU
	// print output buff
	readFromDevice(commandQueue, devOutput, hostOutput, sizeof(int) * globalSize);
#endif

#ifdef PRINTOUTPUT
	for (int i = 0; i < globalSize; i++){
		printf(">>> thread %d, Action %d, key %d, output %d\n", i, hostActions[i], hostKeys[i], hostOutput[i]);
	}
#endif

#ifdef CORRECTNESS
	isCorrect(hostKeys, hostOutput, globalSize, linkedListHead, (hostKeys+globalSize)/*this should be the key for CPU*/, CPUresult);
#endif

	// Free memory space
	clSVMFree(context, linkedListHead);
	clSVMFree(context, freeMemPool);
	clSVMFree(context, nextFreeSlot);
	if (hostKeys) free(hostKeys);
	if (hostOutput) free(hostOutput);
	// free(hostAddress);

	// ------- Release OpenCL objects -------
	if(commandQueue)clReleaseCommandQueue(commandQueue);
	if(context)clReleaseContext(context);
	if(program)clReleaseProgram(program);
	if(kernel)clReleaseKernel(kernel);

#if USE_GPU
	if(event)clReleaseEvent(event);
#endif

	if (devOutput) clReleaseMemObject(devOutput);
	// if (devAddress) clReleaseMemObject(devAddress);

	return 0;
}
