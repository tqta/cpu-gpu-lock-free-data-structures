#pragma OPENCL EXTENSION cl_khr_int64_extended_atomics : enable

#define TRUE 1
#define FALSE 0
#define INVALID -99999999
#define NULL 0
#define FAIL -111111
#define SUCCESS 111111

#define DISTRIBUTION20 // add 20%, delete 20%, search 60%, otherwise add 40%, delete 40%, search 20%


typedef struct Node {
	// A node contains: value, address of the next node and mark bit
	// first 48 bits is the address of next node,
	// and the next 16 bits is an unsigned 0 or 1 integer for mark bit
	// (logically but not yet physically removed)
	int value;
	atomic_ulong nextAndMark;
	//struct Node* rep;
} Node;

typedef struct Window {
	Node* pred;
	Node* curr;
} Window;

// ======= Helper functions to get process node address and markbit ==========
unsigned long combine(Node* pointer, unsigned short markbit){
	return ((unsigned long) pointer << 16 | ((unsigned long) markbit));
}

Node* extractPointer(atomic_ulong* nextAndMark){
	// TODO load and shift operation --> atomic???
	unsigned long myNextAndMark = atomic_load_explicit(nextAndMark, memory_order_seq_cst, memory_scope_all_svm_devices);
	return (Node*)(myNextAndMark >> 16);
}

unsigned short extractMarkbit(atomic_ulong* nextAndMark){
	// TODO load and bit-wise AND --> atomic?
	unsigned long myNextAndMark = atomic_load_explicit(nextAndMark, memory_order_seq_cst, memory_scope_all_svm_devices);
	return (unsigned short) (myNextAndMark & 0x000000000000FFFF);
}

// =========== find method ===========
// this method returns a region (window) containing searchKey (if any)
// and cleans up dirty node along the way
Window find(Node* linkedList, int searchKey){
	Node* pred = NULL;
	Node* curr = NULL;
	Node* succ = NULL;

	bool snip;

	retry: pred = linkedList;
	curr = extractPointer(&(pred->nextAndMark));

	Window newWindow;	// returned region (i.e., window)

	do {
		if(curr != NULL) {
			succ = extractPointer(&(curr->nextAndMark));

			while (curr != NULL && extractMarkbit(&(curr->nextAndMark)) == 1) { // as long as current node is marked --> remove it
				// we expect the current node (both address and markbit) is unchanged
				unsigned long expected = combine(curr,0);	// next node is curr and pred is clean
				unsigned long desired = combine(succ,0);	// next node is succ and pred is clean

				// atomically compare our expected (current address + mark bit) with the real (current address + mark bit)
				// if it's as expected --> update the pred->nextAndMark to the succ node (physically remove the curr)
				// otherwise --> snip failed --> retry (i.e., someone else already modified either address or mark bit of the curr)
				snip = atomic_compare_exchange_strong_explicit(&(pred->nextAndMark), &expected, desired, memory_order_seq_cst, memory_order_relaxed, memory_scope_all_svm_devices); // compare value, expected value, desired value;

				if(!snip){
					goto retry;
				}

				// TODO put the removed node to the garbage

				// update curr
				curr = succ;
				if (curr != NULL)
					succ = extractPointer(&(curr->nextAndMark));
			}
		}

		/* after the above loop --> all marked nodes before this point must be removed */

		newWindow.pred = pred;
		newWindow.curr = curr;

		// move on to the next node
		pred = curr;
		if(curr != NULL){
			curr = extractPointer(&(curr->nextAndMark));
		}
	// loop until the previous curr (i.e., pred now) is NULL or its value is greater than or equal to the search key
	} while (pred != NULL && pred->value < searchKey);	//question: used to be while loop, now do-while, used to be curr != NULL, now pred. is it because we changed head node from 0 to head.

	return newWindow;
}

// ========== Search function =============
// return node address if found
// otherwise, return NULL
Node* contains(Node* linkedList, int searchKey){
	// extract the first valid node
	// remember: linkedList is the HEAD (having INVALID value)
	Node* curr = extractPointer(&(linkedList->nextAndMark));

	while ((curr != NULL) && (curr->value < searchKey)){
		// move on to the next node
		curr = extractPointer(&(curr->nextAndMark));
	}

	if ((searchKey == curr->value) && (extractMarkbit(&(curr->nextAndMark)) == 0))
		return curr;
	else
		return (Node*) NULL;
}

// ========== add function =============
int add(Node* linkedList
			, Node* freeMemPool
			, int insertValue
			, atomic_int* nextFreeSlot
			, int* counter
			//, unsigned long* retAddr
){
	// get my slot and update the global nextFreeSlot
	// TODO if I get my slot first, the slot may not be used if the function returns FAIL --> FIX IT
	// if I get my slot after getting the window, each time I retry, new slot will be given to me

	int myslot = atomic_fetch_add_explicit(nextFreeSlot, 1, memory_order_seq_cst, memory_scope_all_svm_devices);

	while (true) {
		// find the region/window (pred, curr) to add the insertValue
		Window window = find(linkedList, insertValue);

		if (window.curr != NULL && window.curr->value == insertValue){
			// the insertValue is already in the linked list
			// no duplicated value is allowed in a set --> failed
			return FAIL;
		}
		/* otherwise, add the insertValue in between pred and curr */

		// write the insertValue to new node
		freeMemPool[myslot].value = insertValue;
		// atomic_store_explicit(&((atomic_int)freeMemPool[myslot].value), insertValue, memory_order_seq_cst, memory_scope_all_svm_devices);
		mem_fence(CLK_GLOBAL_MEM_FENCE);

	//	retAddr[0] = freeMemPool[myslot].value;
	//	retAddr[0] = insertValue;


		// update the nextAndMark field of new node
		unsigned long desired = combine(window.curr,0);
		atomic_store_explicit(&(freeMemPool[myslot].nextAndMark),desired, memory_order_seq_cst, memory_scope_all_svm_devices);

		// link the new node to the linked list
		unsigned long expected = combine(window.curr,0);
		desired = combine(freeMemPool+myslot,0);

		int ret = atomic_compare_exchange_strong_explicit(&(window.pred->nextAndMark), &expected, desired, memory_order_seq_cst, memory_order_relaxed, memory_scope_all_svm_devices);

		if (ret == 0) continue;

		return SUCCESS;
		//(*counter)++;
	}
}

// ========== remove function =============
int removes(Node* linkedList, int removeValue){
 	while (true) {
 		Window window = find(linkedList, removeValue);

		if (window.curr == NULL){
			return FAIL;
		}

 		if (window.curr->value != removeValue){
			// removeValue is not in the list
 			return FAIL;
 		} else {
			Node* succ = extractPointer(&(window.curr->nextAndMark));

			unsigned long expected = combine(succ,0); 	// expect that its link to the next node and its mark bit (i.e., clean) are unchanged
			unsigned long desired = combine(succ,1);	// if as expected, mark the remove node as dirty

			int ret = atomic_compare_exchange_strong_explicit(&(window.curr->nextAndMark), &expected, desired, memory_order_seq_cst, memory_order_relaxed, memory_scope_all_svm_devices);

 			if(ret == 0){
				// start over b/c someone has modified either the link to next node or the mark bit of the remove node
 				continue;
 			}

			/* no need to update the pred - the find() will do that */
			return SUCCESS;
 		}
 	}
 }

// =====linked list functions====
__kernel void linkedlist (	__global Node* linkedList, 			// linked list structure
				__global Node* freeMemPool, 		// free memory pool allocated in CPU. This pool is used for new elements to be added
				__global int* keys, 			// keys to be searched
				__global int* actions,
				__global int* output, 				// output buffer
				// __global unsigned long* retAddr,	// return address per thread (DEBUGGING)
				__global atomic_int* nextFreeSlot	// index of the next free slot in memory pool
				//__global unsigned long* svmAddr
){
	// thread id
	int tid = get_global_id(0);
	int wgid = get_group_id(0);
	int lid = get_local_id(0);

	int globalsize = get_global_size(0);



	//test 0: all threads insert different but sequential value.
	//int insertValue = tid;
	//test : all thread insert tid*2+1 into a populated list with value tid*2 in there.
	//int insertValue = tid*2+1;
	//test 1: all threads insert the same value
	//int insertValue = 10;
	//test 2: all thread inserts a random value , in this case, search keys are random and between 0-1023. This part can be changed in host code.
	int key = keys[tid];
	int act = actions[tid];

	//testcase: threads have different job, depending on their tid. if tid%3 = 1, do add, if tid%3 = 2, do remove, otherwise do search.

	//Action 0: add, Action 1: remove, Action 2: search
	//action results: 1.add fail; 2. add success; 3. remove fail; 4. remove success; 5. search fail; 6. search success.

	if(act == 0){
		int returnValue = add(linkedList, freeMemPool, key, nextFreeSlot, output+tid);
		if (returnValue == FAIL){
			output[tid] = 1;
		}
		else{ output[tid] = 2; }
	}
	else if(act == 1){
		int returnValue = removes(linkedList, key);
		if(returnValue == FAIL){
			output[tid] = 3;
		}
		else{output[tid] = 4;}

	}
	else{
		Node* returnValue = contains(linkedList, key);
		if(returnValue != NULL){
			output[tid] = 6;
		}
		else{output[tid] = 5;}

	}


	//test on divergence impact
	//test 3: insert value are scattered
	//int insertValue = lid * 32 + wgid;
	//test 4: insert value are close together
	//int insertValue = wgid * 32 + lid;







// DEBUG: Functional test for remove() function
//	if (tid % 3 == 0) {
//		output[tid] = remove(linkedList, tid);
//		find(linkedList, tid);
//	}

// DEBUG: Functional test for add() function
//	output[tid] = add(linkedList, freeMemPool, tid, nextFreeSlot);

// DEBUG: Functional test for contains() function
//	Node* foundNode = contains(linkedList, searchKeys[tid]);
//	output[tid] = foundNode->value;
//	retAddr[tid] = (unsigned long) foundNode;

// DEBUG: Functional test for find() function
//	if (tid == 0 || tid == 1) {
//		Window win = find(linkedList, searchKeys[tid]);
//		output[tid] = (win.curr)->value;
//		retAddr[tid] = (unsigned long) win.curr;
//	}
}


//******************** Disjoint set Implementation ***********************
/*
void MakeSet(Node* node){
	node->rep = node;
}

Node* FindRep(Node* node){
	//return node->rep;

	if (node->rep == node)
		return node;
	else
		return FindRep(node->rep);
}
void Union(Node* node1, Node* node2){


	Node* rep1 = FindRep(node1);
	Node* rep2 = FindRep(node2);
	rep1->rep = rep2;

	//if rep1!= rep2
		//update all the rep in first linked list.
}
*/
