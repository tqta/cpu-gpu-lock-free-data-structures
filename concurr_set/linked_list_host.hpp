#include "timer.hpp"
#include <deviceSetup.hpp>
#include <math.h>
#include <time.h>
#include <atomic>
#include <vector>
#include <omp.h>
#include <string>
#include <fstream>
#include <thread>
#include <algorithm> // need to include this library to enable std::for_each
#include <sys/time.h>


#define TRUE 1
#define FALSE 0

#define INVALID -99999999

// -------------------------------------------LIST MANIPULATION---------------------------------------------------
#define MAXKEY 10000  //define the testing cases for various key range: 100, 1000, 10000, 100000
//#define RANDOMACTION // if defined, actions are mixed, otherwise actions are distributed as first section is add, second section is remove, and rest are search. section size depend on distribution ratio.

#define RUNNING_ACTION DO_ADD //if a predefined action is needed, define it here. the following 3 lines tells you which number represent which action.

#define DO_ADD 0
#define DO_REMOVE 1
#define DO_SEARCH 2

#define RANDOMKEYS // if defined, keys for each thread is random, if not, it is sequential.
#define DISTRIBUTION20 // if defined, action distribution between actions are 20, 20, 60 (add, remove, search), otherwise distribution is 40,40,20

#define WGSIZE 64
//#define WGNUM 80// update: this macro may be no longer needed when the first for loop in main() is setup. it may be replaced by an iterator count for a for loop inside main. //64*16 =1024, test NDRANGE size is 1k, 10k, 100k, 1m
#define LISTSIZE 10000//starting list size

#define WORKLOAD 16000 * WGSIZE// total workload shared between cpu and gpu // must be a multiple of 64


// #define CORRECTNESS // code in this segment is only used for debugging purpose and need to be removed once testing is done.

#define AFAIL 100000 //return value of a failed add() performed on CPU.
#define ASUCCESS 200000 //return value of a successful add() performed on CPU.

#define ISATOMIC
#define USEOMP

#define USE_CPU 1
#define USE_GPU 0

#if USE_CPU
	#define CPUSHARE WORKLOAD/2 // CPU share of workload, must be equal to WORKLOAD after adding GPUSHARE
	#define GPUSHARE 0 // GPU share of workload, must be equal to WORKLOAD after adding CPUSHARE
#elif USE_GPU
	#define CPUSHARE 0 // CPU share of workload, must be equal to WORKLOAD after adding GPUSHARE
	#define GPUSHARE WORKLOAD/2 // GPU share of workload, must be equal to WORKLOAD after adding CPUSHARE
#else
	#define CPUSHARE 0.5 * WORKLOAD // CPU share of workload, must be equal to WORKLOAD after adding GPUSHARE
	#define GPUSHARE WORKLOAD - CPUSHARE // GPU share of workload, must be equal to WORKLOAD after adding CPUSHARE
#endif

//TO SEE OUTPUTS
//#define PRINTBEFORE
//#define PRINTAFTER
//#define PRINTOUTPUT


#define CPU_THREADS 4  // defines how many cpu threads are spawned.

//=========================
typedef struct Node{
	// A node contains: value, address of the next node and mark bite4r
	// first 48 bits is the address of next node,
	// and the next 16 bits is an unsigned 0 or 1 integer for mark bit
	// (logically but not yet physically removed)
	int value;
	std::atomic<ulong> nextAndMark;
} Node;

typedef struct Window {
	Node* pred;
	Node* curr;
} Window;

typedef std::vector< std::vector<int> > Matrix;
typedef std::vector<int> Row;

cl_platform_id platform;
cl_context context;
cl_uint deviceCount;
cl_device_id device;
cl_command_queue commandQueue;
cl_program program;
cl_kernel kernel;

size_t kernel_size;
cl_event event;
cl_int err;


//=============================== helper function that combines pointer and markbit ==============================================
// combine an address and mark bit into one single unsigned long number
unsigned long combine(Node* pointer, unsigned short markbit){
	return ((unsigned long) pointer << 16 | ((unsigned long) markbit));
}

Node* extractPointer(std::atomic<ulong>* nextAndMark){
	unsigned long myNextAndMark = atomic_load_explicit(nextAndMark, std::memory_order_relaxed);
	return (Node*)(myNextAndMark >> 16);
}

unsigned short extractMarkbit(std::atomic<ulong>* nextAndMark){
	unsigned long myNextAndMark = atomic_load_explicit(nextAndMark, std::memory_order_relaxed);
	return (unsigned short) (myNextAndMark & 0x000000000000FFFF);
}

//======================================= general helper functions ===================================================================================
double standardDeviation(double data[], int numRun, double average){
	double acc = 0;
	for(int i = 0; i<numRun; i++ ){
		acc += (data[i]-average) * (data[i]-average);
	}

	double result = sqrt(acc / numRun);
	return result;
}

void printList(Node* head){
	Node* curr = head;
	int listsize = 0;
	while (curr != NULL){
		printf("value %d - address %lx - marked bit %d - next node address %lx\n", curr->value, (unsigned long) curr, extractMarkbit(&(curr->nextAndMark)), (unsigned long) extractPointer(&(curr->nextAndMark)));
		curr = extractPointer(&(curr->nextAndMark));
		listsize ++;
	}
	printf("list size : %d", listsize);
}


//=============================== actions on the list, add, remove and search    =================================================
Window find(Node* linkedList, int searchKey){
	Node* pred = NULL;
	Node* curr = NULL;
	Node* succ = NULL;

	bool snip;
	pred = linkedList;

	tryagain: pred = linkedList;
	curr = extractPointer(&(pred->nextAndMark));

	Window newWindow;	// returned region (i.e., window)

	do {
		if(curr != NULL) {
			succ = extractPointer(&(curr->nextAndMark));

			while (curr != NULL && extractMarkbit(&(curr->nextAndMark)) == 1) { // as long as current node is marked --> remove it
				// we expect the current node (both address and markbit) is unchanged
				unsigned long expected = combine(curr,0);	// next node is curr and pred is clean
				unsigned long desired = combine(succ,0);	// next node is succ and pred is clean

				// atomically compare our expected (current address + mark bit) with the real (current address + mark bit)
				// if it's as expected --> update the pred->nextAndMark to the succ node (physically remove the curr)
				// otherwise --> snip failed --> retry (i.e., someone else already modified either address or mark bit of the curr)
				snip = atomic_compare_exchange_strong(&(pred->nextAndMark), &expected, desired); // compare value, expected value, desired value;

				if(!snip){
					goto tryagain;
				}

				// TODO put the removed node to the garbage

				// update curr
				curr = succ;
				if (curr != NULL)
					succ = extractPointer(&(curr->nextAndMark));
			}
		}

		/* after the above loop --> all marked nodes before this point must be removed */

		newWindow.pred = pred;
		newWindow.curr = curr;

		// move on to the next node
		pred = curr;
		if(curr != NULL){
			curr = extractPointer(&(curr->nextAndMark));
		}
	// loop until the previous curr (i.e., pred now) is NULL or its value is greater than or equal to the search key
	} while (pred != NULL && pred->value < searchKey);	//question: used to be while loop, now do-while, used to be curr != NULL, now pred. is it because we changed head node from 0 to head.

	return newWindow;
}

int add(Node* linkedList, Node* freeMemPool, int insertValue, std::atomic<int>* nextFreeSlot/*, int* counter*/){
	// get my slot and update the global nextFreeSlot
	// TODO if I get my slot first, the slot may not be used if the function returns FAIL --> FIX IT
	// if I get my slot after getting the window, each time I retry, new slot will be given to me

	int myslot = atomic_fetch_add_explicit(nextFreeSlot, 1, std::memory_order_relaxed);

	while (true) {
		// find the region/window (pred, curr) to add the insertValue
		Window window = find(linkedList, insertValue);

		if (window.curr != NULL && window.curr->value == insertValue){
			// the insertValue is already in the linked list
			// no duplicated value is allowed in a set --> failed
			return AFAIL;
		}

		/* otherwise, add the insertValue in between pred and curr */

		// write the insertValue to new node
		freeMemPool[myslot].value = insertValue;

		// update the nextAndMark field of new node
		unsigned long desired = combine(window.curr,0);
		atomic_store_explicit(&(freeMemPool[myslot].nextAndMark),desired,std::memory_order_seq_cst);

		// link the new node to the linked list
		unsigned long expected = combine(window.curr,0);
		desired = combine(freeMemPool+myslot,0);

		int ret = atomic_compare_exchange_strong_explicit(&(window.pred->nextAndMark), &expected, desired, std::memory_order_seq_cst, std::memory_order_relaxed);

		if (ret == 0) continue;

		return ASUCCESS;
	}
}

// ============== Initiate SVM objects ==============

// Linked list is shared by both CPU and GPU
Node* createLinkedList(int listSize){
	int err = 0;

	// first node is the HEAD --> its value is INVALID
	int num_node = (listSize + 1);

	// create an SVM array of nodes
#ifdef ISATOMIC
	Node* linkedListHead = (Node*) clSVMAlloc(context, CL_MEM_READ_WRITE|CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS, num_node * sizeof(Node), 0);
#else
	Node* linkedListHead = (Node*) clSVMAlloc(context, CL_MEM_READ_WRITE|CL_MEM_SVM_FINE_GRAIN_BUFFER, num_node * sizeof(Node), 0);
#endif

	printf("list size= %d\n", listSize);

	if (linkedListHead == NULL){
		printf("Error in clSVMAlloc: %s\n", checkError(err));
		exit(-1);
	}

	// instantiate the HEAD
	linkedListHead->value = INVALID;

	atomic_store_explicit(&(linkedListHead->nextAndMark), combine(NULL,0), std::memory_order_relaxed);
	unsigned long val = atomic_load_explicit(&(linkedListHead->nextAndMark), std::memory_order_relaxed);
	Node* val_ptr = extractPointer(&(linkedListHead->nextAndMark));
	if (val_ptr != NULL) exit(-1);

	Node* currNode = linkedListHead;

//fill in value of each element, and make links between elements.
	for (int i = 0; i < listSize; i++){
		int markBit = 0;
		if (i != listSize - 1){
			currNode->nextAndMark = combine(currNode+1,markBit);
			currNode = currNode + 1;
			currNode->value = i;
		} else {
			currNode->nextAndMark = combine(NULL, markBit);
		}
	}


	return linkedListHead;
}

//================================ initialization for linked list. ==============================================================
// Free memory pool is the list of already allocated empty nodes
// if GPU threads need to insert new node --> get allocated memory from here
Node* createFreeMemPool(int poolSize){
	// create an SVM array of nodes
#ifdef ISATOMIC
	return (Node*) clSVMAlloc(context, CL_MEM_READ_WRITE|CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS, poolSize * sizeof(Node), 0);
#else
	return (Node*) clSVMAlloc(context, CL_MEM_READ_WRITE|CL_MEM_SVM_FINE_GRAIN_BUFFER, poolSize * sizeof(Node), 0);
#endif

}

// list of keys to be searched in GPU
int* createKeys(std::string filename, int numKeys){
	int* hostKeys = (int*) malloc (sizeof(int) * numKeys);

	std::ifstream myfile(filename);
//	myfile.open(filename);
	if(!myfile){
		printf("problem opening file!!");
		exit(1);
	}

	for (int i = 0; i < numKeys; i++){
#ifdef RANDOMKEYS
		myfile >> hostKeys[i];
#else
		hostKeys[i] = i%MAXKEY;
#endif
	}
	myfile.close();
	return hostKeys;
}

int* createActions(std::string filename, int numActions){
	//action 0 is add, action 1 is remove, action 2 is search
	int* hostActions = (int*) malloc(sizeof(int) * numActions);

#ifdef RANDOMACTION
	//read in a file that has a mixture of 0(add), 1(remove), 2(search).
  std::ifstream myfile;
  myfile.open(filename);

  for(int i = 0; i < numActions; i ++){
    myfile >> hostActions[i];
  }

  myfile.close();
#else
	//
	for(int i = 0; i < numActions; i++){
		hostActions[i] = RUNNING_ACTION;
	}
#endif
/*
	// action is represented by numbers: add is 0, remove is 1, search is 2
	int addThreashold = 0;
	int removeThreshold;
	int searchThreshold;

#ifdef DISTRIBUTION20
	addThreashold = numActions / 5;
#else
	addThreashold = (numActions / 5) * 2;
#endif
	removeThreshold = addThreashold;
	searchThreshold = numActions - addThreashold - removeThreshold;

#ifdef RANDOMACTION
	int addCounter = 0;
	int removeCounter = 0;
	int searchCounter = 0;


	printf("numActions: %d, addThreashold: %d, removeThreshold: %d, searchThreshold: %d\n", numActions, addThreashold, removeThreshold, searchThreshold);

	for(int i = 0; i < numActions; i++){
		int action;
		int flag = 1;
		while(flag){ // will keep on generating new action value until all threadshold are meet
			action = rand() % 3;
			if(action == DO_ADD){ // action is add
				if(addCounter < addThreashold){
					addCounter ++;
					hostActions[i] = action;
					flag = 0;
				}
			}else if( action == DO_REMOVE){
				if(removeCounter < removeThreshold){
					removeCounter ++;
					hostActions[i] = action;
					flag = 0;
				}
			}else { //action == DO_SEARCH
				if(searchCounter < searchThreshold){
					searchCounter ++;
					hostActions[i] = action;
					flag = 0;
				}
			}
		}
	}
#else
	for(int i = 0; i < numActions; i++){
		if(i< addThreashold){ // this part is for add
			hostActions[i] = DO_ADD;
		}
		else if(i< addThreashold * 2){ // this part is for remove
			hostActions[i] = DO_REMOVE;
		}
		else hostActions[i] = DO_SEARCH; // this part is for search
	}

#endif

*/

	return hostActions;
}

//==================================== check correctness function ===============================================================
void isCorrect(int* hostKeys, int* hostOutput, int globalSize, Node* linkedListHead, int* CPUkeys, int* CPUresult){


		//int threadsEachBin = WGSIZE * WGNUM / MAXKEY * 3;// eg: 100 key values for 100 bins, if using 1000 threads, each bin should have less than or equal to 10 elements, but just to make sure we have enough space, alloc 30 to each bin. // this value can be changed to accomondate. This may cause program to report error if one particular value does not have enough allocated space to store all threads
		Matrix bins(MAXKEY);

//store all threads that got and action for each key in bin[key]
		for(int i = 0; i < WORKLOAD; i++){
			if(i < globalSize){//check GPU result
			bins[hostKeys[i]].push_back(hostOutput[i]); // save the result to corresponding bin(key), and later can check their actions to check correctness.
			}else{//check CPU result
			 	bins[hostKeys[i]].push_back(CPUresult[i-globalSize]);
			}
		}

		// for(int i = 0; i < CPUSHARE; i ++){
		// 	bins[hostKeys[i+globalSize]].push_back(CPUresult[i]);
		// }

		//Action 0: add, Action 1: remove, Action 2: search
		//action results: 1.add fail; 2. add success; 3. remove fail; 4. remove success; 5. search fail; 6. search success.

		int checkKey[MAXKEY]; // this array stores the result if this key should be in the linked list or not. The following for loop will do the checking and store result in this array.
		// this checking method assumes that starting list have sequential keys.
		for(int i = 0; i < MAXKEY; i ++){
			int inList;

			if(i < LISTSIZE){ //check if key is in starting list.
				inList = 1;
			}else inList = 0;

			int addSuccess = 0;
			int removeSuccess = 0;
			//check for result. cound successful add and delete
			for(int j = 0; j < bins[i].size(); j++){
				if(bins[i][j] == 2 || bins[i][j] == ASUCCESS) {
					addSuccess ++;
				}else if(bins[i][j] == 4){
					removeSuccess ++;
				}
			}

			if(addSuccess - removeSuccess == 1 ){ // if add has one more success than remove, then it is in the list. and this list should assume
				if(i<LISTSIZE){
					inList = -100; // if it is in original list, then add and remove should have equal amount or remove have one more success. Then this situation is an error.
				}else inList = 1;
			}else if(addSuccess - removeSuccess == 0){
				if(i<LISTSIZE){
					inList = 1; // if they are equal, list remain the same key. address might change though.
				}else inList = 0;
			}else if(removeSuccess - addSuccess == 1){
				if(i<LISTSIZE){
					inList = 0;
				}else inList = -200; // if it is not in orginal list, and have one more success remove, then report error.
			}else inList = addSuccess - removeSuccess; // all other cases should not happen, report error.
			checkKey[i] = inList;
		}

		for(int i = 0; i < MAXKEY; i ++){ //output error message.
			if(checkKey[i] != 1 && checkKey[i] != 0) printf("Error in key bin %d, with error code %d. \n  _______________________________\n(Error code 0 does not count as an error, it just means this key should not be in list.\n Error code -100 means it is in original list, but add and remove does not have equal amount nor remove have one more success. This is and error.\n Error code -200 means it is not in original list , and have one more success remove. this is an error.\n Other error code equals to addSuccess - removeSuccess\n", i, checkKey[i]);
		}
		printf("if you don't see a message like 'Error in key bin ...' above this line, then this linked list passed first stage test. Yay!!!\n");

		// print out results.
/*
		for(int i = 0; i < MAXKEY; i ++){
			printf("\nbin: %d\n", i);
			for(int j = 0; j < bins[i].size(); j++){
				printf("%d ", bins[i][j]);
			}
		}
*/
		Node* curr = linkedListHead;
		int v = curr->value;

		while (curr != NULL){

			curr = extractPointer(&(curr->nextAndMark));
			if(curr == NULL) break;
			v = curr->value;
			if(v == MAXKEY) break;

			if(checkKey[v] == 1){//check off all the keys in the list. By check off means change the checkKey[key] from 1 to 0, then later on see if checkKey still have 1 left.
				checkKey[v] = 0;
			}else if(checkKey[v] == 0) printf("Error in key bin %d , it should not be in list!!!!!\n", v);


		}

		for(int i = 0; i < MAXKEY; i ++){ //output error message.
			if(checkKey[i] == 1) printf("Error in key bin %d, it should be checked off. \n", i); // if we still have unchecked key in the list, then report error.
		}

		printf("if you don't see any error message, then linkedlist passed test! Phew!!!!!\n");

}
