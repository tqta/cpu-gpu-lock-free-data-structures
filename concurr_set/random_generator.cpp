#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include "linked_list_host.hpp"

int main(){
  FILE * key;
  FILE * action;
  srand(time(NULL));

  std::stringstream filename;
  filename << "./randomeKeys_" << std::to_string(MAXKEY) << ".txt";

  key = fopen(filename.str().c_str(), "w");
  action = fopen("./randomeActions.txt", "w");

  if (key == NULL){fprintf(stderr, " Can't open file randomeKeys"); exit(1);}
  if (action == NULL){fprintf(stderr, " Can't open file randomeActions"); exit(1);}

  for(int i = 0; i < WORKLOAD; i++){
    fprintf(key, "%d\n",rand()%MAXKEY );
  }

  /*
  	// action is represented by numbers: add is 0, remove is 1, search is 2
  	int addThreashold = 0;
  	int removeThreshold;
  	int searchThreshold;

  #ifdef DISTRIBUTION20
  	addThreashold = numActions / 5;
  #else
  	addThreashold = (numActions / 5) * 2;
  #endif
  	removeThreshold = addThreashold;
  	searchThreshold = numActions - addThreashold - removeThreshold;

  #ifdef RANDOMACTION
  	int addCounter = 0;
  	int removeCounter = 0;
  	int searchCounter = 0;


  	printf("numActions: %d, addThreashold: %d, removeThreshold: %d, searchThreshold: %d\n", numActions, addThreashold, removeThreshold, searchThreshold);

  	for(int i = 0; i < numActions; i++){
  		int action;
  		int flag = 1;
  		while(flag){ // will keep on generating new action value until all threadshold are meet
  			action = rand() % 3;
  			if(action == 0){ // action is add
  				if(addCounter < addThreashold){
  					addCounter ++;
  					hostActions[i] = action;
  					flag = 0;
  				}
  			}else if( action == 1){
  				if(removeCounter < removeThreshold){
  					removeCounter ++;
  					hostActions[i] = action;
  					flag = 0;
  				}
  			}else {
  				if(searchCounter < searchThreshold){
  					searchCounter ++;
  					hostActions[i] = action;
  					flag = 0;
  				}
  			}
  		}
  	}
  #else
  	for(int i = 0; i < numActions; i++){
  		if(i< addThreashold){ // this part is for add
  			hostActions[i] = 0;
  		}
  		else if(i< addThreashold * 2){ // this part is for remove
  			hostActions[i] = 1;
  		}
  		else hostActions[i] = 2; // this part is for search
  	}

  #endif
*/
  for(int i = 0; i < WORKLOAD; i++){
    fprintf(action, "%d\n", rand()%3); // there are only 3 actions, 0 for add, 1 for remove, 2 for search
  }

  fclose(key);
  fclose(action);


  std::ifstream myfile;
  myfile.open("./randomeActions.txt");

  int* output = (int*) malloc(sizeof(int) * 100);
  for(int i = 0; i < 100; i ++){
    myfile >> output[i];
  }

  myfile.close();
  return 0;
}
